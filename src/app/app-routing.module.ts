import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NuesslerComponent} from './nuesslerModul/nuessler/nuessler.component';
import {ResultPageComponent} from './nuesslerModul/result-page/result-page.component';
import {EinsatzmittelverwaltungComponent} from './einsatzmittelverwaltungModul/components/einsatzmittelverwaltung/einsatzmittelverwaltung.component';
import {EinsatzverwaltungComponent} from './einsatzverwaltungModul/components/einsatzverwaltung/einsatzverwaltung.component';
import {EinsatzstellenuebersichtComponent} from './einsatzstellenuebersichtModul/components/einsatzstellenuebersicht/einsatzstellenuebersicht.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'einsatzverwaltung',
    pathMatch: 'full'
    // component: EinsatzverwaltungComponent
  },
  {
    path: 'nuessler-page',
    component: NuesslerComponent
  },
  {
    path: 'result-page',
    component: ResultPageComponent
  },
  {
    path: 'einsatzmittelverwaltung',
    component: EinsatzmittelverwaltungComponent
  },
  {
    path: 'einsatzverwaltung',
    component: EinsatzverwaltungComponent
  },
  {
    path: 'einsatzstellenuebersicht',
    component: EinsatzstellenuebersichtComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
