import {AfterContentChecked, AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Input} from '../../../POJOs/Input';
import {Selection} from '../../../POJOs/Selection';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {HttpClient} from '@angular/common/http';
import {AppComponent} from '../../app.component';
import {environment} from '../../../environments/environment';
import {Einsatz} from "../../../POJOs/Einsatz";


export interface DialogData {
  errorUN: string;
}

export interface Config {
  retVal: boolean;
}


@Component({
  selector: 'app-nuessler',
  templateUrl: './nuessler.component.html',
  styleUrls: ['./nuessler.component.scss']
})
export class NuesslerComponent implements OnInit {

  get input(): Input {
    return this._input;
  }

  TKchecked = false;
  VBchecked = false;
  einsaetze;
  einsatznummer: number;

  // @ts-ignore
  @ViewChild('stepper') stepper: MatStepper;
  private json: string;
  errorUN: number;
  einsatzText: string;

  constructor(
    // tslint:disable-next-line:variable-name
    public _input: Input,
    private router: Router,
    public dialog: MatDialog,
    public http: HttpClient) {
    AppComponent.toolbarName = 'Nüssler';
  }



  ngOnInit() {
    this._input.vollbrand = false;
    this._input.tiefkalt = false;
    this.input.hausnr = '';
    this.input.postleitzahl = '';
    this.input.ort = '';
    this.input.strasse = '';
    this.http.get(environment.serverUrl + 'getEinsatzInProgress').subscribe((retVal: any) => {
      this.einsaetze = retVal.retVal;
    });
  }


  move(index: number, button: string, val: boolean) {
    this.stepper.selectedIndex = index;
    if (button !== 'back') {
      this.putInMap(button, val);
    }
  }

  resetMap() {
    this.stepper.reset();
    this._input.selection = new Selection();
  }


  submitRequest() {
    let subData;
    let checkData;
    subData = this._input.postleitzahl + '|' + this._input.ort + '|' + this._input.strasse + '|' + this._input.hausnr + '|' + JSON.stringify(this._input.selection) + '|' + this.einsatznummer + '|' + this._input.tiefkalt + '|' + this._input.vollbrand + '|' + this._input.menge + '|' + this._input.unNumb + '|' + 1;
    this.http.get(`http://localhost:5000/checkUN?un=${this._input.unNumb}`)
      .subscribe((data: Config) => {
        checkData = data.retVal;
        this.router.navigate(['result-page'], {state: {data: subData}});
      }, (error => {
        this.errorUN = this._input.unNumb;
        this.openDialog();
      }));
  }

  submit(val: boolean) {
    this._input.selection.brand = val;
    this._input.postleitzahl = (document.getElementById('plz') as HTMLInputElement).value;
    this._input.ort = (document.getElementById('ort') as HTMLInputElement).value;
    this._input.strasse = (document.getElementById('strasse') as HTMLInputElement).value;
    this._input.hausnr = (document.getElementById('hausnr') as HTMLInputElement).value;
    this._input.unNumb = Number((document.getElementById('gefahrnummer') as HTMLInputElement).value);
    this._input.menge = Number((document.getElementById('menge') as HTMLInputElement).value);
    this.submitRequest();
  }

  putInMap(button: string, val: boolean) {
    switch (button) {
      case 'nebel':
        this._input.selection.nebel = val;
        break;
      case 'starkWind':
        this._input.selection.starkWind = val;
        break;
      case 'bedeckt':
        this._input.selection.bedeckt = val;
        break;
      case 'tag':
        this._input.selection.tag = val;
        break;
      case 'leichtWind':
        this._input.selection.leichtWind = val;
        break;
      case 'sehrLeichtWind':
        this._input.selection.sehrLeichtWind = val;
        break;
      case 'fruehlingSommer':
        this._input.selection.fruehlingSommer = val;
        break;
      case 'brand':
        this._input.selection.brand = val;
        break;
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(errorDialog, {
      width: '300px',
      data: {errorUN: this.errorUN}
    });
  }

  einsatzSelected(ev) {
    this.http.get(environment.serverUrl + `getEinsatzById/${this.einsatznummer}`).subscribe((res: any) => {
      this.input.hausnr = res.retVal[0].hausnummer.toString();
      this.input.postleitzahl = res.retVal[0].PLZ.toString();
      this.input.ort = res.retVal[0].ortstadt;
      this.input.strasse = res.retVal[0].strasse;
    });
  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'errorDialog',
  templateUrl: 'errorDialog.html',
})
// tslint:disable-next-line:class-name component-class-suffix
export class errorDialog {

  constructor(
    public dialogRef: MatDialogRef<errorDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onClick(): void {
    this.dialogRef.close();
  }

}


