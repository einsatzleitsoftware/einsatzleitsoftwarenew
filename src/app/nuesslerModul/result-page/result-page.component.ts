import {AfterContentChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {Radius} from '../../../POJOs/Radius';
import {Router} from '@angular/router';
import {Selection} from '../../../POJOs/Selection';
import jsPDF, {AcroFormTextField} from 'jspdf';
import DateTimeFormat = Intl.DateTimeFormat;
import {AppComponent} from '../../app.component';
import {NuesslerBerechnung} from '../../../Service/NuesslerBerechnungen';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {Input} from '../../../POJOs/Input';
import {getHeader} from '../../../Service/ImageService';
import {environment} from '../../../environments/environment';
import {dateString} from '../../../Service/DateService';




@Component({
  selector: 'app-main-page',
  templateUrl: './result-page.component.html',
  styleUrls: ['./result-page.component.scss']
})


export class ResultPageComponent implements OnInit {

  dataSource: Array<{}> = [];
  displayedColumns: string[] = ['wetterlage', 'auswahl'];
  latitude: number;
  longitude: number;
  markerLatitude: number;
  markerLongitude: number;
  originalLatitude: number;
  originalLongitude: number;
  lockMap = true;
  mapType = 'hybrid';
  zoom = 15;
  nBerechnung = new NuesslerBerechnung();
  tmpInput  = new Input();

  lockCircles = true;
  private res;
  private _klasse = 'Klasse: ';
  private innenaussen;
  private _berechnung;
  private _winkel;
  private _menge;
  private massfactor;
  private _un;
  private _pac;
  private postleitzahl;
  private ort;
  private strasse;
  private hausnr;
  private einsatznummer: number;
  private einsatzText;
  private tiefkalt;
  private vollbrand;
  private staticMap;
  private staticMapCanvas;
  // tslint:disable-next-line:variable-name
  _radius: Radius = new Radius();
  loaded: Promise<boolean>;
  berechnungLoaded: Promise<boolean>;
  selection: Selection;
  center: google.maps.LatLngLiteral;
  mapCenter: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: this.mapType,
    scrollwheel: true
  };
  markerOptions: google.maps.MarkerOptions = {
    draggable: this.lockCircles,
    clickable: false
  };
  circleAussenOptions: google.maps.CircleOptions = {
    strokeColor: '#FFFF00',
    strokeWeight: 1,
    fillColor: '#FFFF00',
    fillOpacity: 0.35,
    draggable: false,
    editable: false
  };
  circleInnenOptions: google.maps.CircleOptions = {
    strokeColor: '#FF0000',
    strokeWeight: 1,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    draggable: false,
    editable: false
  };



  constructor(private router: Router,
              public http: HttpClient) {
    AppComponent.toolbarName = 'Nüssler Ergebnis';
  }


  ngOnInit() {

    this.berechnungLoaded = Promise.resolve(false);
    // tslint:disable-next-line:triple-equals
    if (history.state.data != undefined) {
      this.res = history.state.data.split('|');
    } else {
      this.res = null;
    }

    this.postleitzahl = this.res[0];
    this.ort = this.res[1];
    this.strasse = this.res[2];
    this.hausnr = this.res[3];
    this.selection = JSON.parse(this.res[4]) as Selection;
    this.einsatznummer = Number(this.res[5]);
    this.tiefkalt = this.res[6];
    this.vollbrand = this.res[7];
    this._menge = Number(this.res[8]);
    this._un = this.res[9];

    // document.getElementById('klasse').innerHTML = this.klasse.toUpperCase();
    this.tmpInput.selection = this.selection;
    this.tmpInput.menge = this._menge;
    this.tmpInput.unNumb = this._un;
    this.tmpInput.vollbrand = this.vollbrand;
    this.tmpInput.tiefkalt = this.tiefkalt;
    this.tmpInput.hausnr = this.hausnr;
    this.tmpInput.postleitzahl = this.postleitzahl;
    this.tmpInput.ort = this.ort;
    this.tmpInput.strasse = this.strasse;

    this._klasse = this.nBerechnung.specialCase(this.nBerechnung.getClass(this.selection), this.tmpInput).toUpperCase();

    this.http.get(`http://localhost:5000/getPAC?un=${this._un}`).subscribe((data) => {
      this._pac = (data as any).retVal;
      this.http.get(`http://localhost:5000/getMF?un=${this._un}`).subscribe((data1) =>  {
        this.massfactor = (data1 as any).retVal;
        this.http.get(`http://localhost:5000/getWinkel?kl=${this._klasse.toLowerCase()}`).subscribe(async (data2) => {
          this._winkel = (data2 as any).retVal;
          const tmpMenge = this.nBerechnung.calcMenge(Number(this._menge) * Number(this.massfactor));
          const tmpGrenze = this.nBerechnung.calcPAC(this._pac);
          await this.nBerechnung.finalValue(this._klasse, tmpGrenze, tmpMenge, this.tmpInput, this._pac, this.massfactor).then((response) => {
            this._berechnung = this.nBerechnung.berechnung;
            this._radius.werta = response.werta;
            this._radius.werti = response.werti;



            this.berechnungLoaded = Promise.resolve(true);
            this.addressToLatLong(this.postleitzahl, this.ort, this.strasse, this.hausnr);

            /*document.getElementById('werti').innerHTML = String(Math.round(response.werti) + ' m');
            document.getElementById('werta').innerHTML = String(Math.round(response.werta) + ' m');
            document.getElementById('berechnung').innerHTML = this._berechnung;
            document.getElementById('winkel').innerHTML = String(this._winkel) + '°';
            document.getElementById('men').innerHTML = String(this._menge) + ' kg';
            // document.getElementById('un').innerHTML = this.un;
            document.getElementById('pac').innerHTML = this._pac;*/
            this.staticMap = document.getElementById('staticMap');
            this.staticMapCanvas = document.getElementById('staticMapCanvas');
            if (response.werta < 100 && response.werta > 0) {
              this.zoom = 19;
            } else if (response.werta < 200 && response.werta >= 100) {
              this.zoom = 18;
            } else if (response.werta < 400 && response.werta >= 200) {
              this.zoom = 17;
            } else if (response.werta < 800 && response.werta >= 400) {
              this.zoom = 16;
            } else if (response.werta < 1600 && response.werta >= 800) {
              this.zoom = 15;
            } else if (response.werta < 3200 && response.werta >= 1600) {
              this.zoom = 14;
            } else if (response.werta < 6400 && response.werta >= 3200) {
              this.zoom = 13;
            } else if (response.werta < 12800 && response.werta >= 6400) {
              this.zoom = 12;
            } else if (response.werta < 25600 && response.werta >= 12800) {
              this.zoom = 11;
            } else if (response.werta < 51200 && response.werta >= 25600) {
              this.zoom = 10;
            }
            this.dataSource[0] = ({wetterlage: 'Nebel?', auswahl: this.selection._nebel ? 'JA' : 'NEIN'});
            this.dataSource[1] = {wetterlage: 'Starker Wind?', auswahl: this.selection._starkWind ? 'JA' : 'NEIN'};
            this.dataSource[2] = {wetterlage: 'Himmel bedeckt?', auswahl: this.selection._bedeckt ? 'JA' : 'NEIN'};
            this.dataSource[3] = {wetterlage: 'Tag?', auswahl: this.selection._tag ? 'JA' : 'NEIN'};
            this.dataSource[4] = {wetterlage: 'Leichter Wind?', auswahl: this.selection._leichtWind ? 'JA' : 'NEIN'};
            this.dataSource[5] = {wetterlage: 'Sehr leichter Wind?', auswahl: this.selection._sehrLeichtWind ? 'JA' : 'NEIN'};
            this.dataSource[6] = {wetterlage: 'Frühling/Sommer', auswahl: this.selection._fruehlingSommer ? 'JA' : 'NEIN'};
            this.dataSource[7] = {wetterlage: 'Brand?', auswahl: this.selection._brand ? 'JA' : 'NEIN'};

            if (Number(this.res[10]) === 1) {
              const body = {
                ergAussen: this._radius.werta,
                ergInnen: this._radius.werti,
                berechnung: this.berechnung,
                winkel: this.winkel,
                klasse: this.klasse,
                zeitstempel: new Date(Date.now()),
                einsatznummer: this.einsatznummer,
                unNummer: this.un,
                menge: this.menge,
                selection: JSON.stringify(this.selection),
                vollbrand: this.vollbrand,
                tiefkalt: this.tiefkalt
              };

              if (this.einsatznummer) {
                this.http.post(environment.serverUrl + 'postNuesslerErgebnis', body).subscribe(() => {
                });
              }
            }
            if (this.einsatznummer) {
              this.http.get(`${environment.serverUrl}getEinsatzByID/${this.einsatznummer}`).subscribe((retVal: any) => {
                const erg = retVal.retVal[0];
                this.einsatzText = `${erg.einsatzstichwort} ${erg.notfallgeschehen}
              ${erg.strasse}`;
              });
            } else {
              this.einsatzText = '';
            }
          });
        });

      } );


    });

  }

  addressToLatLong(postleitzahl, ort, strasse, hausnr) {
    const that = this;
    const xhr = new XMLHttpRequest();
    let data;
    // tslint:disable-next-line:max-line-length
    xhr.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + postleitzahl + '+' + ort + '+' + strasse + '+' + hausnr + '&key=' + environment.apiKey, true);
    xhr.send();
    // tslint:disable-next-line:only-arrow-functions
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          that.loaded = Promise.resolve(true);
          data = xhr.responseText;
          const json = JSON.parse(data);
          that.setLatLong(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng);
        }
      }
    };
  }

  goBack() {
    this.router.navigate(['nuessler-page']);
  }

  fixCircles() {
    this.lockCircles = !this.lockCircles;
  }

  generatePDF() {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
      // format: [297, 210]
    });
    const imgData = getHeader();
    const t = new DateTimeFormat();
    const d = new Date(Date.now());

    doc.addImage(imgData, 'PNG', 10, 5, 190, 30);
    if (this.einsatzText !== '') {
      doc.text('Einsatz: ' + this.einsatzText, 10, 45);
    }
    else {
      doc.text('Einsatz: ', 10, 45);
      const einsatzField = new AcroFormTextField();
      einsatzField.x = 35;
      einsatzField.y = 39;
      einsatzField.height = 8;
      einsatzField.width = 50;
      doc.addField(einsatzField);
    }
    doc.text('Datum: ' + t.format(d), 130, 45);
    doc.text('Uhrzeit: ' + d.getHours() + ':' + ((d.getMinutes() < 9) ? `0${d.getMinutes()}` : d.getMinutes()), 130, 53);
    doc.text('Wetter: ', 10, 65);
    const wetterField = new AcroFormTextField();
    wetterField.x = 35;
    wetterField.y = 59;
    wetterField.height = 8;
    wetterField.width = 50;
    doc.addField(wetterField);
    doc.text('Temperatur: ', 105, 65);
    const tempField = new AcroFormTextField();
    tempField.x = 140;
    tempField.y = 59;
    tempField.width = 20;
    tempField.height = 8;
    doc.addField(tempField);
    doc.text('Eingaben:', 10, 80);
    doc.text('UN-Nummer: ' + this._un, 38, 80);
    doc.text('Menge: ' + this._menge, 38, 88);
    doc.text('Stoffname: ', 105, 80);

    const fsize = doc.getFontSize();

    doc.setFontSize(10);
    let h = 88;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.dataSource.length; i++) {
      // @ts-ignore
      let tmp;
      // @ts-ignore
      // tslint:disable-next-line:triple-equals
      if (this.dataSource[i].auswahl == true || this.dataSource[i].auswahl === 'JA') {
        tmp = 'JA';
      } else {
        tmp = 'NEIN';
      }

      // @ts-ignore
      doc.text('- ' + this.dataSource[i].wetterlage.replace('?', ': ') + ' ' + tmp, 105, h);
      h += 5;
    }
    doc.setFontSize(fsize);

    doc.text('Ergebnis:', 10, 140);
    doc.text('Ausbreitungstabelle: Klasse ' + this._klasse.toUpperCase(), 38, 140);
    doc.text('PAC-2: ' + this._pac + ' ppm', 140, 140);
    doc.text('Ausbreitungsradius Innen: ' + this._radius.werti + ' m', 38, 148);
    doc.text('Ausbreitungsradius Außen: ' + this._radius.werta + ' m', 38, 156);
    doc.text('Ausbreitungswinkel: ' + this._winkel + '°', 38, 164);
    doc.text('Mass Factor: ' + this.massfactor, 140, 148);
    doc.text('korrigierte Menge: ' + this._menge * this.massfactor, 140, 156);
    doc.text('Bemerkungen:', 10, 172);
    doc.text('Ersteller: ', 10, 283);

    doc.addPage();
    doc.setFontSize(10);
    const re = /<br>/gi;
    const re2 = /&szlig;/gi;
    let text = this._berechnung.replace(re, '\n');
    text = text.replace(re2, 'ß');


    let retVal1 = '';
    let retVal2 = '';

    this.getBase64Innen(dataUrl => {
      retVal1 = dataUrl;
    });
    this.getBase64Außen(dataUrl => {
      retVal2 = dataUrl;
    });

    setTimeout(() => {
      doc.setFontSize(10);
      doc.setFont('Helvetica', 'bold');
      doc.text('Karte für Evakuierungen von Personen im Inneren von Häusern', 10, 10);
      doc.addImage(retVal1.replace(/^data:image\/(png|jpg);base64,/, ''), 'png', 10, 13, 130, 130);
      doc.text('Karte für Evakuierungen von Personen im Freien', 10, 150);
      doc.addImage(retVal2.replace(/^data:image\/(png|jpg);base64,/, ''), 'png', 10, 153, 130, 130);
      doc.addPage();
      doc.setFont('Helvetica', 'normal');
      doc.text(text, 10, 10, {maxWidth: 185});


      doc.line(10, 270, 60, 270);
      doc.line(80, 270, 130, 270);
      doc.line(150, 270, 200, 270);

      doc.save(`Nuesslerergebnis_${dateString(new Date(Date.now()))}.pdf`);
    }, 2000);


  }

  getBase64Innen(callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', this.staticMapFunctionInnerCircle());
    xhr.responseType = 'blob';
    xhr.send();
  }

  getBase64Außen(callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      const reader = new FileReader();
      // tslint:disable-next-line:only-arrow-functions
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', this.staticMapFunctionOuterCircle());
    xhr.responseType = 'blob';
    xhr.send();
  }


  staticMapFunctionOuterCircle() {

    const $circRadius =       this._radius.werta / 1000;         // Radius in km
    const $circRadiusThick =  5;

    const circlePoints = this.getCirclePoints(this.latitude, this.longitude, $circRadius); // Encoded polyline string
    let $encString = '';
    for (const i of circlePoints) {
      $encString += `${Number(i.lat).toFixed(5)},${Number(i.long).toFixed(5)}|`;
    }
    $encString = $encString.slice(0, $encString.length - 1);
    let url = 'https://maps.googleapis.com/maps/api/staticmap?';
    url += 'center=' + this.latitude + ',' + this.longitude + '&';
    url += 'zoom=' + (this.zoom - 1) + '&';
    url += 'size=' + 500 + 'x' + 500 + '&';
    url += 'maptype=hybrid&';
    url += 'style=feature:water|element:geometry.fill|color:0x9bd3ff&';
    url += 'path=';
    url += 'color:0xFFFF00|';
    url += 'fillcolor:0xFFFF00|';
    url += 'weight:' + $circRadiusThick + '|';
    url +=  $encString + '&';
    url += 'key=' + environment.apiKey;
    return url;
  }


  staticMapFunctionInnerCircle() {

    const $circRadius =       this._radius.werti / 1000;         // Radius in km
    const $circRadiusThick =  5;

    const circlePoints = this.getCirclePoints(this.latitude, this.longitude, $circRadius); // Encoded polyline string
    let $encString = '';
    for (const i of circlePoints) {
      $encString += `${Number(i.lat).toFixed(5)},${Number(i.long).toFixed(5)}|`;
    }
    $encString = $encString.slice(0, $encString.length - 1);


    let url = 'https://maps.googleapis.com/maps/api/staticmap?';
    url += 'center=' + this.latitude + ',' + this.longitude + '&';
    url += 'zoom=' + this.zoom + '&';
    url += 'size=' + 500 + 'x' + 500 + '&';
    url += 'maptype=hybrid&';
    url += 'style=feature:water|element:geometry.fill|color:0x9bd3ff&';
    url += 'path=';
    url += 'color:0xFF0000|';
    url += 'fillcolor:0xFF0000|';
    url += 'weight:' + $circRadiusThick + '|';
    url +=  $encString + '&';
    url += 'key=' + environment.apiKey;
    return url;
  }

  getCirclePoints(lat, long, circRadius) {

    const circlePoints = [];

    // tslint:disable-next-line:variable-name
    const lat_rad = (lat * Math.PI) / 180;
    // tslint:disable-next-line:variable-name
    const long_rad = (long * Math.PI) / 180;
    const dist = circRadius / 6371;

    for (let deg = 0; deg < 360; deg += 5) {
      const rad = (deg * Math.PI) / 180;

      // tslint:disable-next-line:variable-name
      const new_lat = Math.asin(Math.sin(lat_rad) * Math.cos(dist) + Math.cos(lat_rad) * Math.sin(dist) * Math.cos(rad));
      // tslint:disable-next-line:variable-name max-line-length
      const new_long = long_rad + Math.atan2(Math.sin(rad) * Math.sin(dist) * Math.cos(lat_rad), Math.cos(dist) - Math.sin(lat_rad) * Math.sin(new_lat));

      // tslint:disable-next-line:variable-name
      const new_lat_deg = (new_lat * 180) / Math.PI;
      // tslint:disable-next-line:variable-name
      const new_long_deg = (new_long * 180) / Math.PI;
      circlePoints.push({lat: new_lat_deg, long: new_long_deg});
    }
    return circlePoints;
  }


  get klasse(): string {
    return this._klasse;
  }

  set klasse(value: string) {
    this._klasse = value;
  }

  get berechnung() {
    return this._berechnung;
  }

  set berechnung(value) {
    this._berechnung = value;
  }

  get winkel() {
    return this._winkel;
  }

  set winkel(value) {
    this._winkel = value;
  }

  get menge() {
    return this._menge;
  }

  set menge(value) {
    this._menge = value;
  }

  get un() {
    return this._un;
  }

  set un(value) {
    this._un = value;
  }

  get pac() {
    return this._pac;
  }

  set pac(value) {
    this._pac = value;
  }



  setCircle($event: any) {
    this.center = {
      lat: $event.latLng.lat(),
      lng: $event.latLng.lng()
    };
    this.latitude = $event.latLng.lat();
    this.longitude = $event.latLng.lng();
  }


  setLatLong(lat, long) {
    this.mapCenter = {
      lat,
      lng: long
    };
    this.center = {
      lat,
      lng: long
    };
    this.markerLatitude = lat;
    this.markerLongitude = long;
    this.latitude = lat;
    this.longitude = long;
    this.originalLatitude = lat;
    this.originalLongitude = long;
  }



  returnToOriginalPosition() {
    this.center = {
      lat: this.originalLatitude,
      lng: this.originalLongitude
    };
    this.markerLongitude = this.originalLongitude;
    this.markerLatitude = this.originalLatitude;
    this.latitude = this.originalLatitude;
    this.longitude = this.originalLongitude;
  }


}
