import {Component, Inject, OnInit} from '@angular/core';
import {Feuerwehr} from '../../../../POJOs/Feuerwehr';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import {openErrorDialog} from '../../../../Service/DialogService';

interface StatusZuteilung {
  id: number;
  text: string;
}

@Component({
  selector: 'app-einsatzmittel-anlegen-dialog',
  templateUrl: './einsatzmittel-anlegen-dialog.component.html',
  styleUrls: ['./einsatzmittel-anlegen-dialog.component.scss']
})
export class EinsatzmittelAnlegenDialogComponent implements OnInit {
  title: string;
  create: boolean;
  selectedStatus: number;
  selectedFeuerwehr: number;
  feuerwehren: Feuerwehr[];
  status: StatusZuteilung;
  delLogData: { text: string, title: string };
  requiredData = ['Fahrzeugname', 'Kurzbezeichnung', 'Insassen'];


  constructor(public http: HttpClient, public dialog: MatDialogRef<EinsatzmittelAnlegenDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public delLog: MatDialog) {
    this.create = this.data.fahrzeug == null;
    this.create ? this.title = 'Fahrzeug anlegen' : this.title = 'Einsatzmittel bearbeiten';
  }

  async ngOnInit(): Promise<void> {
    this.feuerwehren = this.data.feuerwehren;
    this.status = this.data.status;
    if (!this.create) {
      (document.getElementById('Fahrzeugname') as HTMLInputElement).value = '' + this.data.fahrzeug.einsatzmittelName;
      (document.getElementById('Kurzbezeichnung') as HTMLInputElement).value = '' + this.data.fahrzeug.kurzbezeichnung;
      (document.getElementById('Insassen') as HTMLInputElement).value = '' + this.data.fahrzeug.insassen;
      this.selectedStatus = this.data.fahrzeug.einsatzmittelStatusID;
      this.selectedFeuerwehr = this.data.fahrzeug.feuerwehrID;
      (document.getElementById('Notizen') as HTMLInputElement).value = '' + this.data.fahrzeug.notizen;
    }
    else {
      this.selectedStatus = this.status[0].id;
      document.getElementById('delBtn').style.display = 'none';
    }
  }

  saveData(): void {
    const checkData = this.checkRequired();
    if (checkData[0]) {
      let id;
      this.data.fahrzeug !== undefined ? id = this.data.fahrzeug.einsatzmittelID : id = 0;
      const tmp =
        new Einsatzmittel(
          id,
          (document.getElementById('Fahrzeugname') as HTMLInputElement).value,
          (document.getElementById('Kurzbezeichnung') as HTMLInputElement).value,
          Number((document.getElementById('Insassen') as HTMLInputElement).value),
          this.selectedStatus,
          this.selectedFeuerwehr,
          (document.getElementById('Notizen') as HTMLInputElement).value,
        );
      if (!this.create) {
        tmp.einsatzmittelStatusID = this.selectedStatus;
        tmp.feuerwehrID = this.selectedFeuerwehr;
      }
      this.dialog.close(tmp);
    }
    else {
      openErrorDialog(checkData[1], this.delLog);
    }
  }

  checkRequired(): [found: boolean, errString: string] {
    let errStr = 'Die Felder:';
    let retVal = true;
    for (const str of this.requiredData) {
      if ((document.getElementById(str) as HTMLInputElement).value === '') {
        retVal = false;
        errStr += (' ' + str + ',');
      }
    }
    if (this.selectedFeuerwehr === undefined) {
      retVal = false;
      errStr += (' Feuerwehr,');
    }

    errStr = errStr.slice(0, errStr.length - 1);
    errStr += ' sind Pflichtfelder!';

    return [retVal, errStr];
  }

  delete() {
    const text = 'Wollen Sie dieses Einsatzmittel wirklich löschen?';
    const title = 'Einsatzmittel löschen';
    this.delLogData = {text, title};
    const dialogRef = this.delLog.open(LoeschDialogComponent, {
      width: '370px',
      height: '200px',
      data: this.delLogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.dialog.close([this.data.fahrzeug.einsatzmittelID, this.data.fahrzeug.einsatzmittelStatusID, true]);
      }
    });
  }
}
