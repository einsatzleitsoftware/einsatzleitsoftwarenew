import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Feuerwehr} from '../../../../POJOs/Feuerwehr';
import {openErrorDialog} from '../../../../Service/DialogService';

@Component({
  selector: 'app-feuerwehr-anlegen-dialog',
  templateUrl: './feuerwehr-anlegen-dialog.component.html',
  styleUrls: ['./feuerwehr-anlegen-dialog.component.scss']
})
export class FeuerwehrAnlegenDialogComponent implements OnInit {
  create: boolean;
  title: string;

  constructor(public http: HttpClient, public dialog: MatDialogRef<FeuerwehrAnlegenDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public delLog: MatDialog) {
    this.create = this.data.feuerwehr == null;
    this.create ? this.title = 'Feuerwehr anlegen' : this.title = 'Feuerwehr bearbeiten';
  }

  async ngOnInit(): Promise<void> {
    if (!this.create) {
      (document.getElementById('Feuerwehr') as HTMLInputElement).value = '' + this.data.feuerwehr.ort;
    }
  }

  saveData(): void {
    const checkData = this.checkRequired();
    if (checkData[0]) {
      let id;
      this.data.feuerwehr !== undefined ? id = this.data.feuerwehr.feuerwehrID : id = 0;
      const tmp =
        new Feuerwehr(
          id,
          (document.getElementById('Feuerwehr') as HTMLInputElement).value,
          1,
        );
      this.dialog.close(tmp);
    } else {
      openErrorDialog(checkData[1], this.delLog);
    }
  }

  checkRequired(): [found: boolean, errString: string] {
    let errStr = null;
    let retVal = true;
    if ((document.getElementById('Feuerwehr') as HTMLInputElement).value === '') {
      retVal = false;
      errStr = 'Das Feld Feuerwehrname ist ein Pflichtfeld!';
    }
    return [retVal, errStr];
  }
}
