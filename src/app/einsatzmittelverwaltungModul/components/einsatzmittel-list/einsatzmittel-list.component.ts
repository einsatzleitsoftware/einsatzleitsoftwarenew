import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {AppComponent} from '../../../app.component';
import {Feuerwehr} from '../../../../POJOs/Feuerwehr';
import {EinsatzmittelAnlegenDialogComponent} from '../../dialogues/einsatzmittel-anlegen-dialog/einsatzmittel-anlegen-dialog.component';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import jsPDF from 'jspdf';
import {getHeader} from '../../../../Service/ImageService';
import autoTable from 'jspdf-autotable';
import {formatDate} from '../../../../Service/DateService';
import {environment} from '../../../../environments/environment';
import {dateString} from '../../../../Service/DateService';
import {openErrorDialog} from '../../../../Service/DialogService';
import {MatTableDataSource} from '@angular/material/table';
import {EinsatzmittelData} from '../EinsatzmittelDataService/einsatzmittelData';
import {EinsatzmittelDataService} from '../EinsatzmittelDataService/einsatzmittelDataService';
import {Einsatz} from '../../../../POJOs/Einsatz';

interface StatusZuteilung {
  id: number;
  text: string;
}

@Component({
  selector: 'app-einsatzmittel-liste',
  templateUrl: './einsatzmittel-list.component.html',
  styleUrls: ['./einsatzmittel-list.component.scss']
})
export class EinsatzmittelListeComponent implements OnInit {
  columns: string[] = ['einsatzmittelName', 'kurzbezeichnung', 'insassen', 'einsatzmittelStatusID', 'feuerwehrID', 'details', 'delete'];
  data: Einsatzmittel[] = [];
  dataSource = new MatTableDataSource(this.data);
  isLoadingData = true;
  einstzmittelDataService: EinsatzmittelData = new EinsatzmittelDataService(this.http);

  feuerwehren: Feuerwehr[] = [];
  status: StatusZuteilung[] = [
    {id: 1, text: 'im Einsatz'},
    {id: 2, text: 'verfügbar'},
    {id: 3, text: 'nicht verfügbar'}
  ];
  statusAnlegen: StatusZuteilung[] = [
    {id: 2, text: 'verfügbar'},
    {id: 3, text: 'nicht verfügbar'}
  ];
  delLogData: { text: string, title: string };


  @ViewChild('paginatorEinsatzmittel') paginatorEinsatzmittel: MatPaginator;
  @ViewChild('sortEinsatzmittel') sortEinsatzmittel: MatSort;

  constructor(public http: HttpClient, public dialog: MatDialog) {
    AppComponent.toolbarName = 'Einsatzmittelverwaltung';
  }

  ngOnInit(): void {
    this.http.get(`http://localhost:5000/getFeuerwehren`).subscribe((feuerwehr: any) => {
      this.feuerwehren = feuerwehr.retVal;
    }, this.reportError);
    this.loadData();
  }

  reportError(err) {
    console.log(err);
  }

  loadData(): void{
    this.einstzmittelDataService.getRequest().subscribe((einsatzmittel: any) => {
      this.data = einsatzmittel.retVal;
      this.dataSource.data = this.data;
      this.dataSource.sort = this.sortEinsatzmittel;
      this.paginatorEinsatzmittel.pageSize = 100;
      this.dataSource.paginator = this.paginatorEinsatzmittel;
      this.isLoadingData = false;
    }, this.reportError);
  }

  getStatus(id) {
    const index = this.status.findIndex(item => item.id === id);
    return this.status[index].text;
  }

  getFeuerwehr(id) {
    const retVal: Feuerwehr[] = this.feuerwehren.filter(feuerwehr => feuerwehr.feuerwehrID === id);
    return retVal[0].ort;
  }

  getColorOfStatus(ele) {
    switch (ele.einsatzmittelStatusID) {
      case 3:
        return 'grey';
      case 2:
        return 'blue';
      case 1:
        return 'red';
    }
  }

  fahrzeugAnlegen(status: StatusZuteilung[]): void {
    this.http.get(`http://localhost:5000/getFeuerwehren`).subscribe((feuerwehr: any) => {
      this.feuerwehren = feuerwehr.retVal;
      const dialogRef = this.dialog.open(EinsatzmittelAnlegenDialogComponent, {
        data: {feuerwehren: this.feuerwehren, status},
        width: '450px',
        height: '550px',
        disableClose: true
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        if (result) {
          this.insertBehaviour(result);
        }
      });
    }, this.reportError);
  }

  insertBehaviour(einsatzmittel: Einsatzmittel) {
    this.einstzmittelDataService.postRequest(einsatzmittel).subscribe((retVal: any) => {
      einsatzmittel.einsatzmittelID = retVal.retVal;
      this.data.push(einsatzmittel);
      this.dataSource.data = this.data;
    }, this.reportError);
  }

  fahrzeugBearbeiten(fahrzeug: Einsatzmittel, status: StatusZuteilung[]): void {
    this.http.get(`http://localhost:5000/getFeuerwehren`).subscribe((feuerwehr: any) => {
      this.feuerwehren = feuerwehr.retVal;
      const dialogRef = this.dialog.open(EinsatzmittelAnlegenDialogComponent, {
        data: {fahrzeug, feuerwehren: this.feuerwehren, status},
        width: '450px',
        height: '550px',
        disableClose: true
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          if (result[2] === true) {
            this.deleteEinsatzmittel(result);
          } else {
            this.updateBehaviour(result);
          }
        }
      });
    }, this.reportError);
  }

  deleteFromEinsatzmittelList(element: Einsatzmittel) {
    const text = 'Wollen Sie dieses Einsatzmittel wirklich löschen?';
    const title = 'Einsatzmittel löschen';
    this.delLogData = {text, title};
    const dialogRef = this.dialog.open(LoeschDialogComponent, {
      width: '370px',
      height: '200px',
      data: this.delLogData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteEinsatzmittel([element.einsatzmittelID, element.einsatzmittelStatusID]);
      }
    });
  }

  deleteEinsatzmittel(result: any) {
    if (result[1] === 1) {
      openErrorDialog('Dieses Einsatzmittel ist noch im Einsatz!', this.dialog);
    } else {
      const res = {
        einsatzmittelID: result[0]
      };
      this.deleteBehavior(res);
    }
  }

  updateBehaviour(einsatzmittel: Einsatzmittel) {
    this.einstzmittelDataService.putRequest(einsatzmittel).subscribe((retVal: any) => {
      for (let i = 0; i < this.data.length; i++) {
        if (this.data[i].einsatzmittelID === einsatzmittel.einsatzmittelID) {
          this.data[i] = einsatzmittel;
        }
      }
      this.dataSource.data = this.data;
    }, this.reportError);
  }

  deleteBehavior(res: any){
    this.einstzmittelDataService.deleteRequest(res, this.data).subscribe((retVal: any) => {
      this.data = this.data.filter(ele => ele.einsatzmittelID !== res.einsatzmittelID);
      this.dataSource.data = this.data;
    }, this.reportError);
  }

  generateEinsatzmittelliste(): void {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
    });

    const imgData = getHeader();
    doc.addImage(imgData, 'PNG', 10, 5, 190, 25);
    doc.text('Einsatzmittelliste - Stand: ' + dateString(new Date(Date.now())), 15, 45);
    // @ts-ignore
    const tab: [[string, number, string, string]] = [];
    for (const ele of this.data) {
      tab.push([ele.kurzbezeichnung + ' - ' + ele.einsatzmittelName, ele.insassen, this.getStatus(ele.einsatzmittelStatusID), this.getFeuerwehr(ele.feuerwehrID)]);
    }

    autoTable(doc, {
      startY: 55,
      theme: 'grid',
      headStyles: {fillColor: [172, 37, 37]},
      styles: {minCellHeight: 12, fontSize: 12, valign: 'middle', halign: 'center'},
      head: [['Einsatzmittelname', 'maximale Insassen', 'Status', 'Feuerwehr']],
      body: tab,
    });

    doc.save('Einsatzmittelliste_' + formatDate(new Date(Date.now())) + '.pdf');
  }
}
