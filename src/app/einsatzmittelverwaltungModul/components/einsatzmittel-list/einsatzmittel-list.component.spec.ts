import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EinsatzmittelListeComponent} from './einsatzmittel-list.component';
import {MAT_DIALOG_DATA, MAT_DIALOG_SCROLL_STRATEGY, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {MatCard, MatCardContent} from '@angular/material/card';
import {Overlay} from '@angular/cdk/overlay';
import {MatSortModule} from '@angular/material/sort';
import {MAT_PAGINATOR_DEFAULT_OPTIONS, MatPaginatorModule} from '@angular/material/paginator';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {AppComponent} from '../../../app.component';
import {EinsatzmittelDataMock} from '../EinsatzmittelDataService/einsatzmittelDataMock';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {Feuerwehr} from '../../../../POJOs/Feuerwehr';

describe('EinsatzmittelListeComponent', () => {
  let component: EinsatzmittelListeComponent;
  let fixture: ComponentFixture<EinsatzmittelListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EinsatzmittelListeComponent],
      imports: [MatSortModule, MatPaginatorModule, BrowserAnimationsModule],
      providers: [HttpClient, HttpHandler, MatDialog, Overlay, {provide: MAT_DIALOG_DATA, useValue: {}},
        {provide: MatDialogRef, useValue: {}}, {
          provide: MAT_DIALOG_SCROLL_STRATEGY,
          useValue: {}
        }, MatCard, MatCardContent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EinsatzmittelListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.einstzmittelDataService = new EinsatzmittelDataMock(component.http);
    component.data.push(new Einsatzmittel(100, 'Tanklöschfahrzeug', 'TLF', 5, 2, 2));
    component.data.push(new Einsatzmittel(200, 'Kleinlöschfahrzeug', 'KLF', 3, 2, 2));
    component.feuerwehren.push(new Feuerwehr(2, 'Dobersdorf Ort', 1));
  });

  it('title', () => {
    expect(AppComponent.toolbarName).toEqual('Einsatzmittelverwaltung');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('status red', () => {
    expect(component.getColorOfStatus(component.data[0])).toBe('red');
  });

  it('status verfügbar', () => {
    expect(component.getStatus(component.data[0].einsatzmittelStatusID)).toBe('verfügbar');
  });

  it('feuerwehr richtig', () => {
     expect(component.getFeuerwehr(component.data[0].feuerwehrID)).toEqual('Dobersdorf Ort');
  });

  it('TestDeleteBehaviour', () => {
    component.deleteBehavior({einsatzmittelID: 200});
    console.log(component.data);
    expect(component.data.length).toEqual(1);
  });

  it('TestGetBehaviour', () => {
    component.data = component.data.filter(() => false);
    expect(component.data.length).toBe(0);
    component.loadData();
    expect(component.data.length).toBe(2);
  });

  it('TestInsertBehaviour', () => {
    component.insertBehaviour(new Einsatzmittel(777, 'Tanklöschfahrzeug', 'TLF', 5, 2, 2));
    expect(component.data.length).toBe(3);
    expect(component.data[2].einsatzmittelID).toBe(666);
  });

  it('TestUpdateBehaviour', () => {
    expect(component.data[1].kurzbezeichnung).toBe('KLF');
    component.updateBehaviour(new Einsatzmittel(200, 'Tanklöschfahrzeug', 'MTF', 5, 2, 2));
    expect(component.data[1].kurzbezeichnung).toBe('MTF');
  });
});
