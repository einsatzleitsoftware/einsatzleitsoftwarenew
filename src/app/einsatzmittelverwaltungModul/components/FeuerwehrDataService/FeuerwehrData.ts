import {Observable} from 'rxjs';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {Feuerwehr} from "../../../../POJOs/Feuerwehr";

interface FeuerwehrData {
  deleteRequest(delId: any, feuerwehrList: Feuerwehr[]): Observable<any>;
  getRequest(): Observable<any>;
  postRequest(feuerwehr: Feuerwehr): Observable<any>;
  putRequest(feuerwehr: Feuerwehr): Observable<any>;
}

export {FeuerwehrData};
