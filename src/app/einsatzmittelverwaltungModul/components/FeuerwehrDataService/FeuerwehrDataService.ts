import {FeuerwehrData} from "./FeuerwehrData";
import {HttpClient} from "@angular/common/http";
import {Feuerwehr} from "../../../../POJOs/Feuerwehr";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

class FeuerwehrDataService implements FeuerwehrData {
  constructor(private http: HttpClient) {
  }

  deleteRequest(delId: any, feuerwehrList: Feuerwehr[]): Observable<any> {
    return this.http.put(`${environment.serverUrl}deleteFeuerwehr`, delId);
  }

  getRequest(): Observable<any> {
    return this.http.get(`${environment.serverUrl}getFeuerwehren`);
  }

  postRequest(feuerwehr: Feuerwehr): Observable<any> {
    return this.http.post<Feuerwehr>(`${environment.serverUrl}postFeuerwehr`, feuerwehr);
  }

  putRequest(feuerwehr: Feuerwehr): Observable<any> {
    return this.http.put<Feuerwehr>(`${environment.serverUrl}putFeuerwehr`, feuerwehr);
  }
}

export { FeuerwehrDataService };
