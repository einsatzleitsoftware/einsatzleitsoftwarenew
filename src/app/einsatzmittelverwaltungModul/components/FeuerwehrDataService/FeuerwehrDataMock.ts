import {EinsatzmittelData} from "../EinsatzmittelDataService/einsatzmittelData";
import {HttpClient} from "@angular/common/http";
import {FeuerwehrData} from "./FeuerwehrData";
import {Feuerwehr} from "../../../../POJOs/Feuerwehr";
import {Observable, of} from "rxjs";
import {Einsatz} from "../../../../POJOs/Einsatz";

class FeuerwehrDataMock implements FeuerwehrData {
  constructor(private http: HttpClient) {
  }

  deleteRequest(delId: any, feuerwehrList: Feuerwehr[]): Observable<any> {
    const tmp = of(feuerwehrList.filter(ele => ele.feuerwehrID !== delId.retVal));
    return tmp;
  }

  getRequest(): Observable<any> {
    let tmp: Feuerwehr[] = [];
    tmp.push(new Feuerwehr(99990, 'Rudersdorf', 1));
    tmp.push(new Feuerwehr(99991, 'Dobersdorf', 1));
    tmp.push(new Feuerwehr(99992, 'Pinkafeld', 2));
    return of({retVal: tmp});
  }

  postRequest(feuerwehr: Feuerwehr): Observable<any> {
    return of({retVal: 123456789});
  }

  putRequest(feuerwehr: Feuerwehr): Observable<any> {
    return of(feuerwehr);
  }
}

export { FeuerwehrDataMock };
