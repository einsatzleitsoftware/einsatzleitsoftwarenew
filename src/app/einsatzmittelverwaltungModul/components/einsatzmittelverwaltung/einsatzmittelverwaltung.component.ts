import { AfterViewInit, Component, Inject, OnInit, QueryList, ViewChild } from '@angular/core';

@Component({
  selector: 'app-einsatzmittelverwaltung',
  templateUrl: './einsatzmittelverwaltung.component.html',
  styleUrls: ['./einsatzmittelverwaltung.component.scss']
})

export class EinsatzmittelverwaltungComponent implements AfterViewInit, OnInit {
  ngOnInit(): void {
  }

  ngAfterViewInit() {
  }
}
