import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EinsatzmittelverwaltungComponent } from './einsatzmittelverwaltung.component';

describe('EinsatzmittelverwaltungComponent', () => {
  let component: EinsatzmittelverwaltungComponent;
  let fixture: ComponentFixture<EinsatzmittelverwaltungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EinsatzmittelverwaltungComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EinsatzmittelverwaltungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
