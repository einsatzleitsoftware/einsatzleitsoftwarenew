import {EinsatzmittelData} from './einsatzmittelData';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {Observable, of} from 'rxjs';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import {Einsatz} from '../../../../POJOs/Einsatz';


class EinsatzmittelDataMock implements EinsatzmittelData {
  constructor(private http: HttpClient) {
  }

  deleteRequest(delId: number, einsatzmittelList: Einsatzmittel[]): Observable<any> {
    const tmp = of(einsatzmittelList.filter(ele => ele.einsatzmittelID !== delId));
    return tmp;
  }

  getRequest(): Observable<any> {
    const hlp: Einsatzmittel[] = [];
    hlp.push(new Einsatzmittel(100, 'Tanklöschfahrzeug', 'TLF', 5, 2, 2));
    hlp.push(new Einsatzmittel(200, 'Kleinlöschfahrzeug', 'KLF', 3, 2, 2));
    return of({retVal: hlp});
  }

  postRequest(einsatzmittel: Einsatzmittel): Observable<any> {
    return of({retVal: 666});
  }

  putRequest(einsatzmittel: Einsatzmittel): Observable<any> {
    return of(einsatzmittel);
  }
}

export {EinsatzmittelDataMock};
