import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';

interface EinsatzmittelData {
  deleteRequest(delId: any, einsatzmittelList: Einsatzmittel[]): Observable<any>;
  getRequest(): Observable<any>;
  postRequest(einsatzmittel: Einsatzmittel): Observable<any>;
  putRequest(einsatzmittel: Einsatzmittel): Observable<any>;
}

export {EinsatzmittelData};
