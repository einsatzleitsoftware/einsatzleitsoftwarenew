import {EinsatzmittelData} from './einsatzmittelData';
import {HttpClient} from '@angular/common/http';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../environments/environment';
import {Einsatzmittel} from '../../../../POJOs/Einsatzmittel';
import {Observable} from 'rxjs';
import {Einsatz} from '../../../../POJOs/Einsatz';


class EinsatzmittelDataService implements EinsatzmittelData {
  constructor(private http: HttpClient) {
  }

  deleteRequest(delId: any, einsatzmittelList: Einsatzmittel[]): Observable<any> {
    return this.http.put(`${environment.serverUrl}deleteEinsatzmittel`, delId);
  }

  getRequest(): Observable<any> {
    return this.http.get(`${environment.serverUrl}getEinsatzmittel`);
  }

  postRequest(einsatzmittel: Einsatzmittel): Observable<any> {
    return this.http.post<Einsatzmittel>(`${environment.serverUrl}postEinsatzmittel`, einsatzmittel);
  }

  putRequest(einsatzmittel: Einsatzmittel): Observable<any> {
    return this.http.put<Einsatzmittel>(`${environment.serverUrl}putEinsatzmittel`, einsatzmittel);
  }
}

export {EinsatzmittelDataService};




