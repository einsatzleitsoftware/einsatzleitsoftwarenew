import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeuerwehrListComponent } from './feuerwehr-list.component';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {Overlay} from '@angular/cdk/overlay';
import {MAT_DIALOG_DATA, MAT_DIALOG_SCROLL_STRATEGY, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatCard, MatCardContent} from '@angular/material/card';
import {MatSortModule} from '@angular/material/sort';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FeuerwehrDataService} from "../FeuerwehrDataService/FeuerwehrDataService";
import {FeuerwehrDataMock} from "../FeuerwehrDataService/FeuerwehrDataMock";
import {Feuerwehr} from "../../../../POJOs/Feuerwehr";
import {Einsatz} from "../../../../POJOs/Einsatz";

describe('FeuerwehrListComponent', () => {
  let component: FeuerwehrListComponent;
  let fixture: ComponentFixture<FeuerwehrListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeuerwehrListComponent ],
      imports: [MatSortModule, MatPaginatorModule, BrowserAnimationsModule],
      providers: [ HttpClient, HttpHandler, MatDialog, Overlay, { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} }, { provide: MAT_DIALOG_SCROLL_STRATEGY, useValue: {} }, MatCard, MatCardContent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeuerwehrListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.feuerwehrService = new FeuerwehrDataMock(component.http);
    component.data.push(new Feuerwehr(99990, 'Rudersdorf', 1));
    component.data.push(new Feuerwehr(99991, 'Dobersdorf', 1));
    component.data.push(new Feuerwehr(99992, 'Pinkafeld', 2));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('TestGetBehaviour', () => {
    component.data = component.data.filter(() => false);
    expect(component.data.length).toBe(0);
    component.loadData();
    expect(component.data.length).toBe(3);
  });

  it('TestInsertBehaviour', () => {
    component.insertBehaviour(new Feuerwehr(4, 'Oberwart', 1));
    expect(component.data.length).toBe(4);
    expect(component.data[3].feuerwehrID).toBe(123456789);
  });

  it('TestUpdateBehaviour', () => {
    expect(component.data[1].ort).toBe('Dobersdorf');
    component.updateBehaviour(new Feuerwehr(99991, 'Tschanigraben', 1));
    expect(component.data[1].ort).toBe('Tschanigraben');
  });

  it('TestDeleteBehaviour', () => {
    component.deleteFeuerwehr(99990);
    expect(component.data.length).toEqual(2);
  });
});
