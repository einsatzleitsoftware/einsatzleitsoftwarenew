import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Feuerwehr} from '../../../../POJOs/Feuerwehr';
import {Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {environment} from '../../../../environments/environment';
import {MatDialog} from '@angular/material/dialog';
import {EinsatzmittelAnlegenDialogComponent} from '../../dialogues/einsatzmittel-anlegen-dialog/einsatzmittel-anlegen-dialog.component';
import {FeuerwehrAnlegenDialogComponent} from '../../dialogues/feuerwehr-anlegen-dialog/feuerwehr-anlegen-dialog.component';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import jsPDF from 'jspdf';
import {getHeader} from '../../../../Service/ImageService';
import autoTable from 'jspdf-autotable';
import {formatDate} from '../../../../Service/DateService';
import {openErrorDialog} from '../../../../Service/DialogService';
import {dateString} from '../../../../Service/DateService';
import {MatTableDataSource} from '@angular/material/table';
import {FeuerwehrData} from '../FeuerwehrDataService/FeuerwehrData';
import {FeuerwehrDataService} from '../FeuerwehrDataService/FeuerwehrDataService';

@Component({
  selector: 'app-feuerwehr-list',
  templateUrl: './feuerwehr-list.component.html',
  styleUrls: ['./feuerwehr-list.component.scss']
})
export class FeuerwehrListComponent implements OnInit {
  columns = ['ort', 'edit', 'delete'];
  // @ts-ignore
  data: Feuerwehr[] = [];
  dataSource = new MatTableDataSource(this.data);
  isLoadingData = true;
  feuerwehrService: FeuerwehrData = new FeuerwehrDataService(this.http);

  delLogData: { text: string, title: string };

  @ViewChild('paginatorFeuerwehren') paginatorFeuerwehren: MatPaginator;
  @ViewChild('sortFeuerwehren') sortFeuerwehren: MatSort;

  constructor(public http: HttpClient, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  reportError(err) {
    console.log(err);
  }

  loadData() {
    this.feuerwehrService.getRequest().subscribe((feuerwehr: any) => {
      this.data = feuerwehr.retVal;
      this.dataSource.data = this.data;
      this.isLoadingData = false;
      this.dataSource.sort = this.sortFeuerwehren;
      this.paginatorFeuerwehren.pageSize = 100;
      this.dataSource.paginator = this.paginatorFeuerwehren;
    }, this.reportError);
  }

  feuerwehrAnlegen(): void {
    const dialogRef = this.dialog.open(FeuerwehrAnlegenDialogComponent, {
      data: {},
      width: '450px',
      height: '250px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.insertBehaviour(result);
      }
    });
  }

  insertBehaviour(feuerwehr: Feuerwehr) {
    this.feuerwehrService.postRequest(feuerwehr).subscribe((retVal: any) => {
      feuerwehr.feuerwehrID = retVal.retVal;
      this.data.push(feuerwehr);
      this.dataSource.data = this.data;
    }, this.reportError);
  }

  feuerwehrBearbeiten(feuerwehr: Feuerwehr): void {
    const dialogRef = this.dialog.open(FeuerwehrAnlegenDialogComponent, {
      data: {feuerwehr},
      width: '450px',
      height: '250px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.updateBehaviour(result);
      }
    });
  }

  updateBehaviour(feuerwehr: Feuerwehr) {
    this.feuerwehrService.putRequest(feuerwehr).subscribe((retVal: any) => {
      for (let i = 0; i < this.data.length; i++) {
        if (this.data[i].feuerwehrID === feuerwehr.feuerwehrID) {
          this.data[i] = feuerwehr;
        }
      }
      this.dataSource.data = this.data;
    }, this.reportError);
  }

  deleteFeuerwehrFromList(element: Feuerwehr) {
    const text = 'Wollen Sie diese Feuerwehr wirklich löschen?';
    const title = 'Feuerwehr löschen';
    this.delLogData = {text, title};
    const dialogRef = this.dialog.open(LoeschDialogComponent, {
      width: '370px',
      height: '200px',
      data: this.delLogData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteFeuerwehr(element.feuerwehrID);
      }
    });
  }

  deleteFeuerwehr(result: any) {
    const res = {
      feuerwehrID: result
    };
    this.deleteBehaviour(res);
  }

  deleteBehaviour(res: any) {
    this.feuerwehrService.deleteRequest(res, this.data).subscribe((retVal: any) => {
      if (retVal) {
        this.data = this.data.filter(ele => ele.feuerwehrID !== res.feuerwehrID);
        this.dataSource.data = this.data;
      } else {
        openErrorDialog(`Es existieren noch Einsatzmittel, die dieser Feuerwehr zugeordnet sind! \n Bitte löschen Sie diese zuerst.`, this.dialog);
      }
    }, this.reportError);
  }

  generateFeuerwehrliste(): void {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
    });

    const imgData = getHeader();
    doc.addImage(imgData, 'PNG', 10, 5, 190, 25);
    doc.text('Feuerwehrliste - Stand: ' + dateString(new Date(Date.now())), 15, 45);
    // @ts-ignore
    const tab: [[string]] = [];
    for (const ele of this.data) {
      tab.push([ele.ort]);
    }

    autoTable(doc, {
      startY: 55,
      theme: 'grid',
      headStyles: {fillColor: [172, 37, 37]},
      styles: {minCellHeight: 12, fontSize: 12, valign: 'middle', halign: 'center'},
      head: [['Feuerwehr']],
      body: tab,
    });

    doc.save('Feuerwehrliste_' + formatDate(new Date(Date.now())) + '.pdf');
  }
}
