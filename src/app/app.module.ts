import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {EinsatzmittelverwaltungComponent} from './einsatzmittelverwaltungModul/components/einsatzmittelverwaltung/einsatzmittelverwaltung.component';
import {
  EinsatzstellenuebersichtComponent,
} from './einsatzstellenuebersichtModul/components/einsatzstellenuebersicht/einsatzstellenuebersicht.component';
import {errorDialog, NuesslerComponent} from './nuesslerModul/nuessler/nuessler.component';
import { ResultPageComponent } from './nuesslerModul/result-page/result-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AppRoutingModule} from './app-routing.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {Input} from '../POJOs/Input';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {GoogleMapsModule} from '@angular/google-maps';
import {MatSelectModule} from '@angular/material/select';
import { LoeschDialogComponent } from './dialogues/loesch-dialog/loesch-dialog.component';
import { AddFzgZuEinsatzDialogComponent } from './dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import { EinsatzOpenListComponent } from './einsatzstellenuebersichtModul/components/einsatz-open-list/einsatz-open-list.component';
import { EinsatzInProgressListComponent } from './einsatzstellenuebersichtModul/components/einsatz-in-progress-list/einsatz-in-progress-list.component';
import { EinsatzEditDialogComponent } from './einsatzstellenuebersichtModul/dialogues/einsatz-edit-dialog/einsatz-edit-dialog.component';
import { LagekarteComponent } from './einsatzstellenuebersichtModul/components/lagekarte/lagekarte.component';
import { EinsatzMarkerDialogComponent } from './einsatzstellenuebersichtModul/dialogues/einsatz-marker-dialog/einsatz-marker-dialog.component';
import { FeuerwehrListComponent } from './einsatzmittelverwaltungModul/components/feuerwehr-list/feuerwehr-list.component';
import { EinsatzmittelListeComponent } from './einsatzmittelverwaltungModul/components/einsatzmittel-list/einsatzmittel-list.component';
import { EinsatzmittelAnlegenDialogComponent } from './einsatzmittelverwaltungModul/dialogues/einsatzmittel-anlegen-dialog/einsatzmittel-anlegen-dialog.component';
import { FeuerwehrAnlegenDialogComponent } from './einsatzmittelverwaltungModul/dialogues/feuerwehr-anlegen-dialog/feuerwehr-anlegen-dialog.component';
import { EinsatzAnlegenComponent } from './einsatzverwaltungModul/dialogues/einsatz-anlegen-component/einsatz-anlegen.component';
import {EinsatzverwaltungComponent} from './einsatzverwaltungModul/components/einsatzverwaltung/einsatzverwaltung.component';
import { ErrorDialogComponent } from './dialogues/error-dialog/error-dialog.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {CommonModule} from '@angular/common';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    EinsatzmittelverwaltungComponent,
    EinsatzstellenuebersichtComponent,
    EinsatzverwaltungComponent,
    NuesslerComponent,
    ResultPageComponent,
    errorDialog,
    LoeschDialogComponent,
    AddFzgZuEinsatzDialogComponent,
    FeuerwehrListComponent,
    EinsatzmittelListeComponent,
    EinsatzmittelAnlegenDialogComponent,
    FeuerwehrAnlegenDialogComponent,
    EinsatzOpenListComponent,
    EinsatzInProgressListComponent,
    EinsatzEditDialogComponent,
    LagekarteComponent,
    EinsatzMarkerDialogComponent,
    EinsatzAnlegenComponent,
    ErrorDialogComponent,
  ],
  entryComponents: [
    errorDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatInputModule,
    AppRoutingModule,
    MatCheckboxModule,
    MatTableModule,
    MatDialogModule,
    MatTooltipModule,
    HttpClientModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    GoogleMapsModule,
    MatSelectModule,
    FormsModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [Input, Selection, MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
