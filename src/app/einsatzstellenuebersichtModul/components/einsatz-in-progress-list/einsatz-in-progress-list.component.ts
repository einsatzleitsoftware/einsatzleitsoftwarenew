import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../environments/environment';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import {EinsatzEditDialogComponent} from '../../dialogues/einsatz-edit-dialog/einsatz-edit-dialog.component';
import {MatTableDataSource} from '@angular/material/table';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {DataService} from '../../../../Service/data.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-einsatz-in-progress-list',
  templateUrl: './einsatz-in-progress-list.component.html',
  styleUrls: ['./einsatz-in-progress-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EinsatzInProgressListComponent implements OnInit, OnDestroy {

  constructor(private http: HttpClient, private dialog: MatDialog, private data: DataService) {
  }


  columns = ['einsatzstichwort', 'fzghinzu', 'edit'];
  einsatzInProgressListe = [];
  dataSource = new MatTableDataSource(this.einsatzInProgressListe);
  isLoadingData = true;
  einsatzElement: Einsatz[] | null;
  subscription: Subscription;


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(() => {
      this.loadData();
    });
    this.loadData();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  loadData() {
    const einsatzArr = [];
    this.http.get(environment.serverUrl + 'getEinsatzInProgress').subscribe((retVal: any) => {
      for (let i = 0; i < retVal.retVal.length; i++) {
        this.http.get(environment.serverUrl + 'getEinsatzmittelzuteilung/' + retVal.retVal[i].einsatznummer).subscribe((retVal1: any) => {

          retVal.retVal[i].zuteilung = retVal1.retVal;
          einsatzArr.push(retVal.retVal[i]);

          if (i === retVal.retVal.length - 1) {
            this.dataSource.data = einsatzArr;
          }
        }, (retVal1: any) => {
          retVal.retVal[i].zuteilung = null;
        });
      }
      this.isLoadingData = false;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    });
  }


  addFzg(einsatz) {
    const dialogRef = this.dialog.open(AddFzgZuEinsatzDialogComponent, {
      data: einsatz,
      width: '600px',
      height: '520px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.loadData();
    });
  }

  editEinsatz(einsatz) {
    const dialogRef = this.dialog.open(EinsatzEditDialogComponent, {
      data: einsatz,
      width: '600px',
      height: '500px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.loadData();
    });
  }

  hausnummerOutput(ele: Einsatz): string {
    if (ele.hausnummer === 0 || isNaN(ele.hausnummer)) {
      return '';
    }
    return ele.hausnummer.toString();
  }
}
