import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import jsPDF from 'jspdf';
import {getHeader} from '../../../../Service/ImageService';
import {dateString, formatDate} from '../../../../Service/DateService';
import {GoogleMap, MapInfoWindow} from '@angular/google-maps';
import {EinsatzMarkerDialogComponent} from '../../dialogues/einsatz-marker-dialog/einsatz-marker-dialog.component';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-lagekarte',
  templateUrl: './lagekarte.component.html',
  styleUrls: ['./lagekarte.component.scss']
})


export class LagekarteComponent implements OnInit, AfterViewInit {

  @ViewChild(GoogleMap, {static: false}) map: GoogleMap;
  @ViewChild(MapInfoWindow, {static: false}) info: MapInfoWindow;
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: false,
    scrollwheel: true,
    disableDoubleClickZoom: true,
    /*maxZoom: 15,
    minZoom: 8,*/
  };
  zoom = 12;
  center: google.maps.LatLngLiteral;
  infoContent = '';
  markers = [];

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.http.get(environment.serverUrl + `getEinsatz`).subscribe((einsätze: any) => {
      for (const einsatz of einsätze.retVal) {
        if (einsatz.einsatzStatusID === 1 || einsatz.einsatzStatusID === 2) {
          this.getMarkerPosition(einsatz);
        }
      }
    }, (err) => console.log(err));
  }

  ngAfterViewInit() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
    }, () => {
      this.center = {
        lat: 47.37537055458556,
        lng: 16.11963589634693,
      };
    });
  }

  getMarkerPosition(einsatz) {
    let color;
    let title;
    if (einsatz.einsatzStatusID === 1) {
      color = 'blue';
    } else if (einsatz.einsatzStatusID === 2) {
      color = 'red';
    }
    title = `${einsatz.einsatzstichwort} ${einsatz.notfallgeschehen}`;
    this.markers.push({
      position: {
        lat: einsatz.lat,
        lng: einsatz.lng
      },
      label: {
        color,
        text: title,
      },
      title,
      options: {
        icon: {url: `http://maps.google.com/mapfiles/ms/micons/${color}-dot.png`},
        draggable: true
      },
      einsatz,
    });

  }


  printMap() {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
      // format: [297, 210]
    });
    const imgData = getHeader();
    doc.addImage(imgData, 'PNG', 1, 1, 210, 25);
    doc.text('Lagekarte - Stand: ' + dateString(new Date(Date.now())), 5, 40);

    let url = `https://maps.googleapis.com/maps/api/staticmap?center=${this.map.getCenter().lat()}, ${this.map.getCenter().lng()}&size=1000x1000&maptype=hybrid&zoom=${this.map.getZoom()}`;
    for (const m of this.markers) {
      url += `&markers=color:${m.label.color}|${m.position.lat},${m.position.lng}`;
    }
    url += `&key=${environment.apiKey}`;
    let base64img;
    this.toDataURL(url, (dataUrl) => {
      base64img = dataUrl;
    });
    setTimeout(() => {
      // @ts-ignore
      doc.addImage(base64img.replace(/^data:image\/(png|jpg);base64,/, ''), 'png', 20, 50);
      doc.save('Lagekarte_' + formatDate(new Date(Date.now())) + '.pdf');
    }, 2000);
  }

  updateMarker(event, marker) {
    this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${event.latLng.lat()},${event.latLng.lng()}&key=${environment.apiKey}`).subscribe((retVal: any) => {
      let hausnr = '';
      let strasse = '';
      let ortStadt = '';
      let PLZ = null;
      let ortStadtFound = false;
      let hausnrFound = false;
      for (const addComp of retVal.results[0].address_components) {
        if (addComp.types[0] === 'street_number') {
          hausnr = addComp.long_name;
          hausnrFound = true;
        } else if (hausnrFound === false) {
          hausnr = null;
        }
        if (addComp.types[0] === 'route') {
          strasse = addComp.long_name;
        }
        if (addComp.types[0] === 'locality') {
          ortStadt = addComp.long_name;
          ortStadtFound = true;
        } else if (addComp.types[0] === 'administrative_area_level_2' && ortStadtFound === false) {
          ortStadt = addComp.long_name;
        }
        if (addComp.types[0] === 'postal_code') {
          PLZ = addComp.long_name;
        }
      }
      const body = {
        PLZ,
        strasse,
        hausnr,
        ortstadt: ortStadt,
        lat: event.latLng.lat(),
        lng: event.latLng.lng(),
        einsatznummer: marker.einsatz.einsatznummer
      };
      this.http.put('http://localhost:5000/putAdresseKoordinaten', body).subscribe(() => {
      }, (err) => console.log(err));
    }, (err) => console.log(err));
  }

  toDataURL(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  onMarkerClick(marker) {
    this.dialog.open(EinsatzMarkerDialogComponent, {
      data: marker,
      width: '450px',
      height: '460px'
    });
  }


}
