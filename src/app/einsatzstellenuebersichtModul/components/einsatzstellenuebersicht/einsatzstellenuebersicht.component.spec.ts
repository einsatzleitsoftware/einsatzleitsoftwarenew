import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EinsatzstellenuebersichtComponent } from './einsatzstellenuebersicht.component';

describe('EinsatzstellenuebersichtComponent', () => {
  let component: EinsatzstellenuebersichtComponent;
  let fixture: ComponentFixture<EinsatzstellenuebersichtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EinsatzstellenuebersichtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EinsatzstellenuebersichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
