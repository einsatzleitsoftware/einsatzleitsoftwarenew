/// <reference types="@types/googlemaps" />
import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {AppComponent} from '../../../app.component';
import {GoogleMap, MapInfoWindow, MapMarker} from '@angular/google-maps';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import jsPDF from 'jspdf';
import {getHeader} from '../../../../Service/ImageService';
import {formatDate} from '../../../../Service/DateService';
import MapsEngineMouseEvent = google.maps.visualization.MapsEngineMouseEvent;


@Component({
  selector: 'app-einsatzstellenuebersicht',
  templateUrl: './einsatzstellenuebersicht.component.html',
  styleUrls: ['./einsatzstellenuebersicht.component.scss']
})
export class EinsatzstellenuebersichtComponent implements OnInit, AfterViewInit {
  constructor() {
    AppComponent.toolbarName = 'Einsatzstellenübersicht';
  }


  ngOnInit(): void {

  }

  ngAfterViewInit() {
  }


}




