import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {environment} from '../../../../environments/environment';
import {MatTableDataSource} from '@angular/material/table';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {Subscription} from 'rxjs';
import {DataService} from '../../../../Service/data.service';

@Component({
  selector: 'app-einsatz-open-list',
  templateUrl: './einsatz-open-list.component.html',
  styleUrls: ['./einsatz-open-list.component.scss']
})
export class EinsatzOpenListComponent implements OnInit, OnDestroy {
  columns = ['einsatzstichwort', 'fzghinzu'];
  einsatzOpenList = [];
  dataSource = new MatTableDataSource(this.einsatzOpenList);
  isLoadingData = true;
  subscription: Subscription;


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private http: HttpClient, private dialog: MatDialog, private data: DataService) {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(message => {
      //this.loadData();
    });
    this.loadData();
  }

  loadData() {
    this.dataSource = new MatTableDataSource();
    this.http.get(environment.serverUrl + 'getEinsatzOpen').subscribe((retVal: any) => {
      this.dataSource.data.push(...retVal.retVal);
      this.isLoadingData = false;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  changeMessage() {
    this.data.changeMessage('hi');
  }

  addFzg(einsatz) {
    const dialogRef = this.dialog.open(AddFzgZuEinsatzDialogComponent, {
      data: einsatz,
      width: '600px',
      height: '520px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
      this.changeMessage();
    });
  }

  /* reload() {
     this.dataProvider.cache = null;
     this.dataSource.load();
   }*/

  hausnummerOutput(ele: Einsatz): string {
    if (ele.hausnummer === 0 || isNaN(ele.hausnummer)) {
      return '';
    }
    return ele.hausnummer.toString();
  }
}
