import {Component, Inject, OnInit} from '@angular/core';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import {HttpClient} from '@angular/common/http';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../environments/environment';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-einsatz-edit-dialog',
  templateUrl: './einsatz-edit-dialog.component.html',
  styleUrls: ['./einsatz-edit-dialog.component.scss']
})

export class EinsatzEditDialogComponent implements OnInit {
  displayedColumns: string[] = ['kurzbezeichnung', 'decreaseInsassen', 'aktuelleInsassen', 'increaseInsassen', 'feuerwehr'];
  maxReached = false;
  minReached = false;
  einsatzMittel = [];
  dataSource = new MatTableDataSource(this.einsatzMittel);
  isLoadingData = true;

  constructor(private http: HttpClient, public dialog: MatDialogRef<EinsatzEditDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public newDialog: MatDialog) {
  }

  ngOnInit() {
    this.http.get(environment.serverUrl + `getEinsatzmittelzuteilung/${this.data.einsatznummer}`).subscribe((retVal: any) => {
      this.einsatzMittel = retVal.retVal;
      this.dataSource.data = this.einsatzMittel;
      this.isLoadingData = false;
    }, (err) => console.log(err));
  }


  increaseInsassen(elem) {
    if (elem.aktuelleInsassen === elem.insassen) {
      elem.maxReached = true;
      elem.minReached = false;
    } else {
      elem.maxReached = false;
      elem.minReached = false;
      elem.aktuelleInsassen++;
    }
  }

  decreaseInsassen(elem) {
    if (elem.aktuelleInsassen <= 0) {
      elem.minReached = true;
      elem.maxReached = false;
    } else {
      elem.minReached = false;
      elem.maxReached = false;
      elem.aktuelleInsassen--;
    }
  }

  openFzgHinzuDialog() {
    this.dialog.close();
    this.newDialog.open(AddFzgZuEinsatzDialogComponent, {
      data: this.data,
      width: '600px',
      height: '460px',
      disableClose: true,
    });
  }

  update() {
    for (const eins of this.einsatzMittel) {
      if (eins.aktuelleInsassen === 0) {
        const res = {
          aktuelleInsassen: eins.aktuelleInsassen,
          zugeteilt: false,
          einsatzmittelID: eins.einsatzmittelID,
          einsatzStatusID: 2
        };
        this.http.put('http://localhost:5000/putAktuelleinsassenZugeteilt', res).subscribe(() => {
          const res1 = {
            einzuID: eins.einsatzmittelzuteilungID,
            endZeit: new Date(Date.now())
          };
          this.http.put('http://localhost:5000/putEinsatzmittelzuteilungEndzeit', res1).subscribe(() => {
            const res2 = {
              emId: eins.einsatzmittelID
            };
            this.http.put('http://localhost:5000/putEinsatzmittelstatus', res2).subscribe(() => {
              this.dialog.close();
            }, (err) => console.log(err));
          }, (err) => console.log(err));
        }, (err) => console.log(err));
      } else {
        const res = {
          aktuelleInsassen: eins.aktuelleInsassen,
          zugeteilt: true,
          einsatzmittelID: eins.einsatzmittelID,
          einsatzStatusID: 1
        };
        this.http.put('http://localhost:5000/putAktuelleinsassenZugeteilt', res).subscribe(() => {
          this.dialog.close();
        }, (err) => console.log(err));
      }
    }
  }

}
