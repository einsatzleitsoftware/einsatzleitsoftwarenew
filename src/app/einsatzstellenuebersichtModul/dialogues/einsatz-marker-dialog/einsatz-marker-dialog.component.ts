import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';

@Component({
  selector: 'app-einsatz-marker-dialog',
  templateUrl: './einsatz-marker-dialog.component.html',
  styleUrls: ['./einsatz-marker-dialog.component.scss']
})
export class EinsatzMarkerDialogComponent implements OnInit {
  title: string;
  notfallgeschehen: string;
  sachverhalt: string;
  einsatzstichwort: string;
  strasse: string;
  hausnr: string;
  PLZ: string;
  ortstadt: string;
  objekt: string;


  constructor(public dialogRef: MatDialogRef<EinsatzMarkerDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog) {
    this.title = 'Einsatzstellenübersicht';
    this.notfallgeschehen = data.einsatz.notfallgeschehen;
    this.sachverhalt = data.einsatz.sachverhalt;
    this.einsatzstichwort = data.einsatz.einsatzstichwort;
    this.strasse = data.einsatz.strasse;
    this.hausnr = data.einsatz.hausnummer;
    this.PLZ = data.einsatz.PLZ;
    this.ortstadt = data.einsatz.ortstadt;
    this.objekt = data.einsatz.objekt;
  }

  ngOnInit(): void {
  }

  openAddFzg() {
    this.dialog.open(AddFzgZuEinsatzDialogComponent, {
      data: this.data.einsatz,
      width: '600px',
      height: '520px',
      disableClose: true,
    });
  }
}
