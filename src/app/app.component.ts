import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {ColorSchemeService} from '../Service/ColorSchemeService';
import {MatSlideToggle, MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, private colorSchemeService: ColorSchemeService) {
    this.matIconRegistry.addSvgIcon(
      'canister',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icon-canister.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icon-light.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'map',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icon-map.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'truck',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icon-truck.svg')
    );
    this.colorSchemeService.load();
  }

  get staticToolbarName() {
    return AppComponent.toolbarName;
  }
  public static toolbarName: string;

  // @ts-ignore
  @ViewChild('stepper') stepper: MatStepper;

  ngOnInit() {
    this.colorSchemeService.update('light');
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
  }

  toggleTheme($event: MatSlideToggleChange) {
    if ($event.checked){
      this.colorSchemeService.update('dark');
    } else {
      this.colorSchemeService.update('light');
    }
  }
}
