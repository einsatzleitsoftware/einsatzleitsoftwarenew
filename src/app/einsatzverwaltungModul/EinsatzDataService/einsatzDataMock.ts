import {LoeschDialogComponent} from "../../dialogues/loesch-dialog/loesch-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {Einsatz} from "../../../POJOs/Einsatz";
import {environment} from "../../../environments/environment";
import {HttpClient} from '@angular/common/http';
import {EinsatzData} from "./einsatzData";
import {Observable, of, Subscription} from "rxjs";
import {log} from "util";

class EinsatzDataMock implements EinsatzData{

  constructor(private http: HttpClient) {
  }

  deleteDialogue(callback: any, dialog: MatDialog) {
    const text = 'Wollen Sie diesen Einsatz wirklich löschen?';
    const title = 'Einsatz löschen';
    const delLogData = {text: text, title: title, callback: callback};
    dialog.open(LoeschDialogComponent, {
      width: '650px',
      height: '200px',
      data: delLogData,
      disableClose: true,
    });
  }

  deleteRequest(delId: number, einsatzList: Einsatz[]): Observable<Einsatz[]> {
    const tmp = of(einsatzList.filter(ele => ele.einsatznummer !== delId));
    return tmp;
  }

  getRequest(): Observable<any> {
    let tmp: Einsatz[] = [];
    tmp.push(new Einsatz(89990, 'T1', 'Fahrzeugbergung', 'Auto gegen Straße gefahren', 'Sirene', 'VW Cross Polo', 'Marbachstraße', 39, 7571, 'Rudersdorf', 2, false, '2021-03-01T12:00:00'));
    tmp.push(new Einsatz(89991, 'B2', 'Hausbrand', 'Erdgeschoss in Flammen', 'Sirene', 'Haus', 'Kuhbergweg', 3, 7571, 'Rudersdorf', 1, false, '2021-02-25T22:12:00'));
    tmp.push(new Einsatz(89992, 'T1', 'Fahrzeugbergung', 'Auto gegen Straße gefahren', 'Sirene', 'VW Cross Polo', 'Marbachstraße', 39, 7571, 'Rudersdorf', 2, false, '2021-03-01T12:00:00'));
    return of({retVal: tmp});
  }

  postRequest(einsatz: Einsatz): Observable<any> {
    return of({einsatznummer: 123456789});
  }

  putRequest(einsatz: Einsatz): Observable<any> {
    return of(einsatz);
  }


}

export {EinsatzDataMock};
