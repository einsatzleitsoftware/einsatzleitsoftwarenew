import {LoeschDialogComponent} from '../../dialogues/loesch-dialog/loesch-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {Einsatz} from '../../../POJOs/Einsatz';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {EinsatzData} from './einsatzData';
import {Observable, of, Subscription} from 'rxjs';

class EinsatzDataService implements EinsatzData{

  constructor(private http: HttpClient) {}

  deleteDialogue(callback: any, dialog: MatDialog) {
    const text = 'Wollen Sie diesen Einsatz wirklich löschen?';
    const title = 'Einsatz löschen';
    const delLogData = {text, title, callback};
    dialog.open(LoeschDialogComponent, {
      width: '650px',
      height: '200px',
      data: delLogData,
      disableClose: true,
    });
  }

  postRequest(einsatz: Einsatz): Observable<any> {
    return this.http.post<Einsatz>(`${environment.serverUrl}postEinsatz`, einsatz);
  }

  deleteRequest(delId: number, einsatzList: Einsatz[]): Observable<any> {
    return this.http.delete(`${environment.serverUrl}deleteEinsatz/${delId}`);
  }

  getRequest(): Observable<any> {
    return this.http.get(`${environment.serverUrl}getEinsatz`);
  }


  putRequest(einsatz: Einsatz): Observable<any> {
    return this.http.put<Einsatz>(`${environment.serverUrl}putEinsatz`, einsatz);
  }
}

export {EinsatzDataService};
