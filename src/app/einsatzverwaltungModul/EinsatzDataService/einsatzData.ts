import {MatDialog} from "@angular/material/dialog";
import {Einsatz} from "../../../POJOs/Einsatz";
import {Observable, Subscription} from "rxjs";

interface EinsatzData {
  deleteDialogue(callback: any, dialog: MatDialog);

  deleteRequest(delId: number, einsatzList: Einsatz[]): Observable<any>;
  getRequest(): Observable<any>;
  postRequest(einsatz: Einsatz): Observable<any>;
  putRequest(einsatz: Einsatz): Observable<any>;
}

export {EinsatzData};
