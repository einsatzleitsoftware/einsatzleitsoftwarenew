import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {AppComponent} from '../../../app.component';
import {HttpClient} from '@angular/common/http';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {MatDialog} from '@angular/material/dialog';
import jsPDF from 'jspdf';
import {getHeader} from '../../../../Service/ImageService';
import {dateString, formatDate} from '../../../../Service/DateService';
import autoTable from 'jspdf-autotable';
import {MatPaginator} from '@angular/material/paginator';
import {AddFzgZuEinsatzDialogComponent} from '../../../dialogues/add-fzg-zu-einsatz-dialog/add-fzg-zu-einsatz-dialog.component';
import {MSGReader} from '../../../../Service/msg.reader';
import {EinsatzAnlegenComponent} from '../../dialogues/einsatz-anlegen-component/einsatz-anlegen.component';
import {environment} from '../../../../environments/environment';
import {openErrorDialog} from '../../../../Service/DialogService';
import {EinsatzDataService} from '../../EinsatzDataService/einsatzDataService';
import {MatTableDataSource} from '@angular/material/table';
import {EinsatzData} from '../../EinsatzDataService/einsatzData';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-einsatzuebersicht',
    templateUrl: './einsatzverwaltung.component.html',
    styleUrls: ['./einsatzverwaltung.component.scss']
  })

export class EinsatzverwaltungComponent implements OnInit {

  einsatzData: Einsatz[] = [];
  dataSource = new MatTableDataSource(this.einsatzData);
  dataService: EinsatzData = new EinsatzDataService(this.http);
  isLoadingData = true;
  displayedColumns: string[] = ['einsatzstichwort', 'einsatzStatusID', 'detail', 'druck', 'loesch', 'addFahrzeug'];
  delLogData: { text: string, title: string };
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  mailColumns: string[] = ['Einsatzstichwort', 'Notfallgeschehen', 'Sachverhalt', 'Alarmiertes Einsatzmittel', 'Objekt', 'Strasse:', 'Strasse / Hs.-Nr.', 'PLZ / Ort', 'Einsatzbeginn(Soll)'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('einsatzSort') einsatzSort: MatSort;

  constructor(public http: HttpClient, public dialog: MatDialog) {
    AppComponent.toolbarName = 'Einsatzverwaltung';
  }

  ngOnInit(): void {
    this.loadData();
  }

  reportError(err) {
    console.log(err);
  }

  loadData() {
    this.dataService.getRequest().subscribe((result) => {
      this.einsatzData = result.retVal;
      this.dataSource.data = this.einsatzData;
      this.isLoadingData = false;
      this.dataSource.sort = this.einsatzSort;
      this.paginator.pageSize = 20;
      this.dataSource.paginator = this.paginator;
    }, this.reportError);
  }

  getColorOfStatus(ele) {
    switch (ele.einsatzStatusID) {
      case 3:
        return 'grey';
      case 2:
        return 'red';
      case 1:
        return 'blue';
      default:
        return '';
    }
  }

  einsatzAnlegen(): void {
    const dialogRef = this.dialog.open(EinsatzAnlegenComponent, {
      width: '650px',
      height: '850px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.setCoordinatesFromAddress(result).then(() => {
          this.insertBehaviour(result);
        });
      }
    });
  }

  insertBehaviour(einsatz: Einsatz) {
    this.dataService.postRequest(einsatz).subscribe((retVal: any) => {
      einsatz.einsatznummer = retVal.einsatznummer;
      this.einsatzData.push(einsatz);
      this.dataSource.data = this.einsatzData;
    }, this.reportError);
  }

  setCoordinatesFromAddress(result: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${result._PLZ}+${result._ortstadt}+${result._strasse}+${result._hausnummer}&key=${environment.apiKey}`).subscribe((retVal: any) => {
        result.lat = retVal.results[0].geometry.location.lat;
        result.lng = retVal.results[0].geometry.location.lng;
        resolve(result);
      }, this.reportError);
    });
  }

  detailsClicked(element: Einsatz): void {
    const dialogRef = this.dialog.open(EinsatzAnlegenComponent, {
      width: '650px',
      height: '850px',
      data: element,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (typeof result === 'object') {
          this.setCoordinatesFromAddress(result).then(() => {
            this.updateBehaviour(result);
          });
        } else {
          this.deleteHttp(result);
        }
      }
    });
  }

  updateBehaviour(einsatz: Einsatz) {
    this.dataService.putRequest(einsatz).subscribe((retVal: any) => {
      for (let i = 0; i < this.einsatzData.length; i++) {
        if (this.einsatzData[i].einsatznummer === einsatz.einsatznummer) {
          this.einsatzData[i] = einsatz;
        }
      }
      this.dataSource.data = this.einsatzData;
    }, this.reportError);
  }

  deleteHttp(result: any) {
    this.dataService.deleteRequest(result, this.einsatzData).subscribe((res) => {
      this.einsatzData = this.einsatzData.filter(ele => ele.einsatznummer !== result);
      this.dataSource.data = this.einsatzData;
    }, this.reportError);
  }

  deleteEinsatzFromList(element: Einsatz) {
    this.dataService.deleteDialogue((toDel: boolean) => {
      if (toDel) {
        if (element.einsatzStatusID === 2) {
          openErrorDialog('Bitte schließen Sie den Einsatz zuvor ab!', this.dialog);
        } else {
          this.deleteHttp(element.einsatznummer);
        }
      }
    }, this.dialog);
  }

  generateEinsatzPDF(ele: Einsatz): void {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
      // format: [297, 210]
    });

    doc.addImage(getHeader(), 'PNG', 10, 5, 190, 25);
    let lsz;
    ele.alarmierendesEinsatzmittel ? lsz = 'Ja' : lsz = 'Nein';

    doc.text('Einsatz: ' + ele.einsatzstichwort + ' ' + ele.notfallgeschehen, 10, 45);
    doc.text('Status: ' + this.getStatusText(ele.einsatzStatusID), 10, 55);
    doc.text('Sachverhalt: ' + ele.sachverhalt, 10, 65);
    doc.text('Adresse: ' + ele.strasse + ' ' + ele.hausnummer, 10, 75);
    doc.text('Ort: ' + ele.PLZ + ', ' + ele.ortstadt, 10, 85);
    doc.text('Objekt: ' + ele.objekt, 10, 95);
    doc.text('Alarmierendes Einsatzmittel: ' + ele.alarmierendesEinsatzmittel, 10, 105);
    doc.text('LSZ-Alarmierung: ' + lsz, 10, 115);

    const eingangstext = dateString(new Date(ele.eingangszeit));
    let starttext;
    let endtext;
    (ele.startzeit === null || ele.startzeit.length < 15) ? starttext = 'Einsatz noch offen' : starttext = dateString(new Date(ele.startzeit));
    (ele.endzeit === null || ele.endzeit.length < 15) ? endtext = 'Einsatz noch nicht abgeschlossen' : endtext = dateString(new Date(ele.endzeit));
    doc.text('Zeiten:', 10, 130);
    doc.text('- Eingangszeit: ' + eingangstext, 32, 130);
    doc.text('- Startzeit: ' + starttext, 32, 140);
    doc.text('- Endzeit: ' + endtext, 32, 150);
    doc.text('Notizen: ', 10, 165);
    doc.text(ele.notizen, 33, 165, {maxWidth: 165});

    // Promise-chain
    this.printEinsatzmittelListe(doc, ele).then((val) => this.printMap(val[0], val[1]));
  }

  printEinsatzmittelListe(doc: jsPDF, ele: Einsatz): Promise<[jsPDF, Einsatz]> {
    return new Promise<[jsPDF, Einsatz]>((resolve, reject) => {
      doc.addPage();
      doc.addImage(getHeader(), 'PNG', 10, 5, 190, 25);
      this.http.get(`${environment.serverUrl}getZuteilungFromEinsatz/${ele.einsatznummer}`).subscribe((retVal: any) => {
        if (retVal.found === true) {
          doc.text('Einsatzmittelliste - Stand: ' + dateString(new Date(Date.now())), 15, 45);
          // @ts-ignore
          const tab: [[string, string, string, string]] = [];
          for (const fzg of retVal.retVal) {
            let zuteilungBisText;
            fzg.zuteilungbis === null ? zuteilungBisText = '-' : zuteilungBisText = dateString(new Date(fzg.zuteilungbis));
            tab.push([fzg.kurzbezeichnung + ' ' + fzg.einsatzmittelName, fzg.ort, dateString(new Date(fzg.zuteilungvon)), zuteilungBisText]);
          }

          autoTable(doc, {
            startY: 55,
            theme: 'grid',
            headStyles: {fillColor: [172, 37, 37]},
            styles: {minCellHeight: 12, fontSize: 12, valign: 'middle', halign: 'center'},
            head: [['Einsatzmittelname', 'Feuerwehr', 'zugeteilt von', 'zugeteilt bis']],
            body: tab,
          });

          resolve([doc, ele]);
        } else {
          reject();
        }
      }, () => {
        doc.text('Keine Fahrzeuge zugeteilt', 15, 45);
        resolve([doc, ele]);
      });
    });
  }

  printMap(doc: jsPDF, ele: Einsatz) {
    doc.addPage();
    doc.addImage(getHeader(), 'PNG', 10, 5, 190, 25);

    let url = `https://maps.googleapis.com/maps/api/staticmap?center=${ele.lat}, ${ele.lng}&size=1000x1000&maptype=hybrid&zoom=19`;
    url += `&markers=color:${this.getColorOfStatus(ele)}|${ele.lat},${ele.lng}`;
    url += `&key=${environment.apiKey}`;
    let base64img;
    this.toDataURL(url, (dataUrl) => {
      base64img = dataUrl;
    });
    setTimeout(() => {
      // @ts-ignore
      doc.addImage(base64img.replace(/^data:image\/(png|jpg);base64,/, ''), 'png', 20, 50);

      this.http.get(`${environment.serverUrl}getNuesslerErgebnisByEinsatzNr/${ele.einsatznummer}`).subscribe((retVal: any) => {
        if (retVal.found) {
          for (const erg of retVal.retVal) {
            doc.addPage();
            doc.addImage(getHeader(), 'PNG', 10, 5, 190, 25);
            doc.setFont('Helvetica', 'bold');
            doc.text(`Nuesslerberechnung Nr. ${erg.nuesslerID}`, 10, 40);
            doc.setFont('Helvetica', 'normal');
            doc.text(`Menge: ${erg.menge} kg`, 10, 50);
            doc.text(`UN-Nummer: ${erg.unNummer}`, 10, 60);
            doc.text(`Ausbreitungswinkel: ${erg.winkel} Grad`, 10, 70);
            doc.text(`Klasse: ${erg.klasse}`, 10, 80);
            doc.text(`Evakuierungsradius im Freien: ${erg.ergebnisAussen} m`, 10, 90);
            doc.text(`Evakuierungsradius im Inneren: ${erg.ergebnisInnen} m`, 10, 100);
            const zeitstempel = dateString(new Date(erg.zeitstempel));
            doc.text(`Berechnungszeit: ${zeitstempel}`, 10, 110);
            doc.setFontSize(12);
            doc.text(`Berechnung: ${erg.berechnung}`, 10, 120, {maxWidth: 185});
            doc.setFontSize(15);
          }
        }
        doc.save('Einsatz_' + ele.einsatznummer + '.pdf');
      });
    }, 2000);
  }

  toDataURL(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  getStatusText(id: number): string {
    switch (id) {
      case 1:
        return 'offen';
      case 2:
        return 'in Arbeit';
      case 3:
        return 'abgeschlossen';
    }
    return '';
  }

  generateListPDF(): void {
    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'mm'
      // format: [297, 210]
    });

    const imgData = getHeader();
    doc.addImage(imgData, 'PNG', 10, 5, 190, 25);
    doc.text('Einsatzliste - Stand: ' + dateString(new Date(Date.now())), 15, 45);
    // @ts-ignore
    const tab: [[string, string, string]] = [];
    for (const ele of this.einsatzData) {
      tab.push([ele.einsatzstichwort + ' ' + ele.notfallgeschehen + ' ' + ele.strasse, ele.eingangszeit.replace('T', ' '), this.getStatusText(ele.einsatzStatusID)]);
    }

    autoTable(doc, {
      startY: 55,
      theme: 'grid',
      headStyles: {fillColor: [172, 37, 37]},
      styles: {minCellHeight: 12, fontSize: 12, valign: 'middle', halign: 'center'},
      head: [['Einsatzname', 'Eingangszeit', 'Status']],
      body: tab,
    });

    doc.save('Einsatzliste_' + formatDate(new Date(Date.now())) + '.pdf');
  }

  addFahrzeug(element: Einsatz) {
    this.dialog.open(AddFzgZuEinsatzDialogComponent, {
      data: element,
      width: '600px',
      height: '460px',
      disableClose: true,
    });
  }

  automaticInsert(msgFile: any) {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      const selectedFile = msgFile.target.files[0];
      if (!selectedFile) {
        return;
      }
      const fileReader = new FileReader();
      fileReader.onload = (evt) => {

        const buffer = evt.target.result;
        const msgReader = new MSGReader(buffer);
        const fileData = msgReader.getFileData();
        if (!fileData.error) {
          const body: string = fileData.body;
          let einsatz = new Einsatz();
          const einArr = [];
          let arr = body.split('\n');

          // @ts-ignore
          arr = arr.filter((l) => l.trim() !== '');
          arr = arr.filter((l) => l.trim() !== ' ');
          const filename: string = selectedFile.name;
          if (filename.includes('Unwettermail')) {
            for (let idx = 0; idx < arr.length; idx++) {
              if (/(^(\d\d\d\d)\D\D\D)/.test(arr[idx])) {
                this.insertDataIntoObject(einsatz, 'PLZ / Ort', arr[idx]);
                this.insertDataIntoObject(einsatz, 'Strasse / Hs.-Nr.', arr[idx - 1]);
                einsatz.einsatznummer = -1;
                einsatz.eingangszeit = formatDate(new Date(Date.now()));
                einsatz.LSZAlarmierung = true;
                einsatz.einsatzStatusID = 1;
                einsatz.sachverhalt = 'Unwetter';
                einsatz.einsatzstichwort = 'Unwetter';
                einArr.push(einsatz);
                einsatz = new Einsatz();
              }
            }
            this.openDia(einArr);

          } else {
            for (const header of this.mailColumns) {
              for (let i = 0; i < arr.length; i++) {
                if (header === arr[i].slice(0, header.length)) {
                  this.insertDataIntoObject(einsatz, header, arr[i + 1]);
                }
              }
            }
            einsatz.einsatznummer = -1;
            einsatz.LSZAlarmierung = true;
            einsatz.einsatzStatusID = 1;
            const dialogRef = this.dialog.open(EinsatzAnlegenComponent, {
              width: '650px',
              height: '850px',
              data: einsatz,
              disableClose: true,
            });

            dialogRef.afterClosed().subscribe((result: any) => {
              if (result) {
                this.setCoordinatesFromAddress(result).then(() => {
                  this.insertBehaviour(result);
                });
              }
            });
          }
        }
        (document.getElementById('fileInput') as HTMLInputElement).value = null;
      };
      fileReader.readAsArrayBuffer(selectedFile);
    }
  }

  async openDia(einsatzarr: Einsatz[]) {
    for (let i = 0; i < einsatzarr.length; i++) {
      const dialogRef = this.dialog.open(EinsatzAnlegenComponent, {
        width: '650px',
        height: '850px',
        disableClose: true,
        data: einsatzarr[i]
      });
      const result = await dialogRef.afterClosed().toPromise();
      if (result) {
        this.setCoordinatesFromAddress(result).then(() => {
          this.insertBehaviour(result);
        });
      }
    }
  }

  insertDataIntoObject(einsatz: Einsatz, header: string, data: string) {
    switch (header) {
      case 'Einsatzstichwort':
        einsatz.einsatzstichwort = data;
        break;
      case 'Notfallgeschehen':
        einsatz.notfallgeschehen = data;
        break;
      case 'Sachverhalt':
        einsatz.notfallgeschehen = data;
        break;
      case 'Alarmiertes Einsatzmittel':
        einsatz.alarmierendesEinsatzmittel = data;
        break;
      case 'Objekt':
        einsatz.objekt = data;
        break;
      case 'Strasse:':
        einsatz.strasse = data;
        break;
      case 'Strasse / Hs.-Nr.':
        const tmp = data.split(/(?=[0-9])(?<=[\s])/);
        if (!isNaN(+tmp[tmp.length - 1])) {
          einsatz.hausnummer = Number.parseInt(tmp[tmp.length - 1], 10);
        }
        let str = '';
        for (let i = 0; i < tmp.length - 1; i++) {
          str += tmp[i];
        }
        einsatz.strasse = str;
        break;
      case 'PLZ / Ort':
        const plzort = data.split(/(?<=[0-9])(?=[\s])/);
        if (!isNaN(+plzort[0])) {
          einsatz.PLZ = Number.parseInt(plzort[0], 10);
        }
        einsatz.ortstadt = plzort[1].trim();
        break;
      case 'Einsatzbeginn(Soll)':
        let tmpStr = '';
        tmpStr += data.slice(6, 10);
        tmpStr += '-';
        tmpStr += data.slice(3, 5);
        tmpStr += '-';
        tmpStr += data.slice(0, 2);
        tmpStr += 'T';
        tmpStr += data.slice(11, 16);
        einsatz.eingangszeit = tmpStr;
        break;
    }
  }

  applyFilter() {
    const from = this.range.controls.start.value;
    let to = this.range.controls.end.value;
    if (to > new Date(Date.now()) || to == null) {
      to = new Date(Date.now());
    }
    if (this.range.controls.start && this.range.controls.end) {
      this.dataSource.data = this.einsatzData.filter(m => {
        return (new Date(m.eingangszeit).getTime()) >= from.getTime() && new Date(m.eingangszeit).getTime() <= to.getTime() + 86400000;
      });
    }
  }

  hausnummerOutput(ele: Einsatz): string {
    if (ele.hausnummer === 0 || isNaN(ele.hausnummer)) {
      return '';
    }
    return ele.hausnummer.toString();
  }
}
