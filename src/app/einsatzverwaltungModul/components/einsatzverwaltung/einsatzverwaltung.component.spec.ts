import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EinsatzverwaltungComponent } from './einsatzverwaltung.component';
import {AppComponent} from "../../../app.component";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {
  MAT_DIALOG_DATA,
  MAT_DIALOG_SCROLL_STRATEGY,
  MatDialog,
  MatDialogModule,
  MatDialogRef
} from "@angular/material/dialog";
import {Overlay} from "@angular/cdk/overlay";
import {InjectionToken, ViewChild} from "@angular/core";
import {MatCard, MatCardContent} from "@angular/material/card";
import {MatSort, MatSortModule} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Einsatz} from "../../../../POJOs/Einsatz";
import {EinsatzDataMock} from "../../EinsatzDataService/einsatzDataMock";
import {log} from "util";

describe('EinsatzverwaltungComponent', () => {
  let component: EinsatzverwaltungComponent;
  let fixture: ComponentFixture<EinsatzverwaltungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EinsatzverwaltungComponent],
      imports: [MatSortModule],
      providers: [HttpClient, HttpHandler, MatDialog, Overlay, {provide: MAT_DIALOG_DATA, useValue: {}},
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_SCROLL_STRATEGY, useValue: {}}, MatCard, MatCardContent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EinsatzverwaltungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.dataService = new EinsatzDataMock(component.http);
    component.einsatzData.push(new Einsatz(99990, 'T1', 'Fahrzeugbergung', 'Auto gegen Straße gefahren', 'Sirene', 'VW Cross Polo', 'Marbachstraße', 39, 7571, 'Rudersdorf', 2, false, '2021-03-01T12:00:00'));
    component.einsatzData.push(new Einsatz(99991, 'B2', 'Hausbrand', 'Erdgeschoss in Flammen', 'Sirene', 'Haus', 'Kuhbergweg', 3, 7571, 'Rudersdorf', 1, false, '2021-02-25T22:12:00'));
    component.einsatzData.push(new Einsatz(99992, 'T1', 'Fahrzeugbergung', 'Auto gegen Straße gefahren', 'Sirene', 'VW Cross Polo', 'Marbachstraße', 39, 7571, 'Rudersdorf', 2, false, '2021-03-01T12:00:00'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('TestEinsatzleitsoftware', () => {
    expect(AppComponent.toolbarName).toEqual('Einsatzverwaltung');
  });

  it('TestStatusColor', () => {
    expect(component.getColorOfStatus(component.einsatzData[0])).toEqual('red');
  });

  it('TestSetCoordinates', () => {
    component.setCoordinatesFromAddress(component.einsatzData[0]).then((result) => {
      expect(result.lat).toEqual(47.065819853069094);
      expect(result.lng).toEqual(16.139600467072324);
    });
  });

  it('TestStatusText', () => {
    expect(component.getStatusText(component.einsatzData[0].einsatzStatusID)).toEqual('in Arbeit');
  });

  it('TestInsertDataInEinsatz', () => {
    component.insertDataIntoObject(component.einsatzData[0], 'Strasse:', 'Bergstraße');
    expect(component.einsatzData[0].strasse).toEqual('Bergstraße');
  });

  it('TestDeleteBehaviour', () => {
    component.deleteHttp(99990);
    expect(component.einsatzData.length).toEqual(2);
  });

  it('TestGetBehaviour', () => {
    component.einsatzData = component.einsatzData.filter(() => false);
    expect(component.einsatzData.length).toBe(0);
    component.loadData();
    expect(component.einsatzData.length).toBe(3);
  });

  it('TestInsertBehaviour', () => {
    component.insertBehaviour(new Einsatz(99994, 'B2', 'Hausbrand', 'Erdgeschoss in Flammen', 'Sirene', 'Haus', 'Kuhbergweg', 3, 7571, 'Rudersdorf', 1, false, '2021-02-25T22:12:00'));
    expect(component.einsatzData.length).toBe(4);
    expect(component.einsatzData[3].einsatznummer).toBe(123456789);
  });

  it('TestUpdateBehaviour', () => {
    expect(component.einsatzData[1].einsatzstichwort).toBe('B2');
    component.updateBehaviour(new Einsatz(99991, 'G3', 'Hausbrand', 'Erdgeschoss in Flammen', 'Sirene', 'Haus', 'Kuhbergweg', 3, 7571, 'Rudersdorf', 1, false, '2021-02-25T22:12:00'));
    expect(component.einsatzData[1].einsatzstichwort).toBe('G3');
  });
});
