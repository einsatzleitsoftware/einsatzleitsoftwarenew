import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Einsatz} from '../../../../POJOs/Einsatz';
import {formatDate} from '../../../../Service/DateService';
import {LoeschDialogComponent} from '../../../dialogues/loesch-dialog/loesch-dialog.component';
import {openErrorDialog} from '../../../../Service/DialogService';
import { EinsatzDataService } from '../../EinsatzDataService/einsatzDataService';
import {environment} from '../../../../environments/environment';
import {Config} from "../../../nuesslerModul/nuessler/nuessler.component";
import {Router} from "@angular/router";

interface StatusZuteilung {
  value: number;
  text: string;
}

interface JaNein {
  value: boolean;
  text: string;
}

@Component({
  selector: 'app-einsatz-anlegen-component',
  templateUrl: './einsatz-anlegen.component.html',
  styleUrls: ['./einsatz-anlegen.component.scss']
})
export class EinsatzAnlegenComponent implements OnInit {
  create: boolean;
  automated = 0;
  title: string;
  btnText: string;
  selectedStatus: number;
  requiredData = ['Einsatzstichwort', 'Eingangszeit', 'PLZ', 'Ort/Stadt'];
  dataService = new EinsatzDataService(this.http);
  selectedNuessler: number;
  nuesslerList: [];

  status: StatusZuteilung[] = [
    {value: 1, text: 'offen'},
    {value: 2, text: 'in Arbeit'},
    {value: 3, text: 'abgeschlossen'}
  ];

  jn: JaNein[] = [
    {value: true, text: 'Ja'},
    {value: false, text: 'Nein'}
  ];

  constructor(public http: HttpClient, public dialog: MatDialogRef<EinsatzAnlegenComponent>, @Inject(MAT_DIALOG_DATA) public data: Einsatz, public delLog: MatDialog, private router: Router) {
    if (data != null) {
      this.automated = data.einsatznummer;
      this.create = false;
      this.automated === -1 ? this.title = 'Einsatz anlegen' : this.title = 'Einsatzdetails';
      this.automated === -1 ? this.btnText = 'Anlegen' : this.btnText = 'Aktualisieren';
    }
    else {
      this.create = true;
      this.title = 'Einsatz anlegen';
      this.btnText = 'Anlegen';
    }

    if (!this.create) {
      this.http.get(`${environment.serverUrl}getNuesslerErgebnisByEinsatzNr/${this.data.einsatznummer}`).subscribe((retVal: any) => {
        if (retVal.found) {
          this.nuesslerList = retVal.retVal;
        }
      });
    }
  }

  ngOnInit(): void {
    if (!this.create) {
      this.updateHTML('Einsatzstichwort', this.data.einsatzstichwort);
      this.updateHTML('notfallgeschehen', this.data.notfallgeschehen);
      this.updateHTML('sachverhalt', this.data.sachverhalt);
      this.updateHTML('alarmierendesEinsatzmittel', this.data.alarmierendesEinsatzmittel);
      this.updateHTML('objekt', this.data.objekt);
      this.updateHTML('strasse', this.data.strasse);
      this.updateHTML('hausnummer', this.data.hausnummer);
      this.updateHTML('PLZ', this.data.PLZ);
      this.updateHTML('Ort/Stadt', this.data.ortstadt);
      this.updateHTML('Eingangszeit', this.data.eingangszeit);
      this.updateHTML('startzeit', this.data.startzeit);
      this.updateHTML('endzeit', this.data.endzeit);
      this.selectedStatus = this.data.einsatzStatusID;
      this.updateHTML('Notizen', this.data.notizen);
    }
    if (this.create || this.automated === -1) {
      document.getElementById('delBtn').style.display = 'none';
      this.updateHTML('Eingangszeit', formatDate(new Date(Date.now())));
    } else {
      const htmlList = document.getElementsByClassName('laufend');
      for (let i = 0; i < htmlList.length; i++) {
        // @ts-ignore
        htmlList.item(i).style.display = 'table';
      }
    }
  }

  updateHTML(key: string, value: any) {
    if (value !== undefined && value !== null) {
      (document.getElementById(key) as HTMLInputElement).value = '' + value;
    }
  }

  saveData(): void {
    const checkData = this.checkRequired();

    if (checkData[0]) {
      let nr;
      this.data !== null ? nr = this.data.einsatznummer : nr = 0;
      const tmp =
        new Einsatz(nr,
          (document.getElementById('Einsatzstichwort') as HTMLInputElement).value,
          (document.getElementById('notfallgeschehen') as HTMLInputElement).value,
          (document.getElementById('sachverhalt') as HTMLInputElement).value,
          (document.getElementById('alarmierendesEinsatzmittel') as HTMLInputElement).value,
          (document.getElementById('objekt') as HTMLInputElement).value,
          (document.getElementById('strasse') as HTMLInputElement).value,
          Number((document.getElementById('hausnummer') as HTMLInputElement).value),
          Number((document.getElementById('PLZ') as HTMLInputElement).value),
          (document.getElementById('Ort/Stadt') as HTMLInputElement).value,
          1, false,
          (document.getElementById('Eingangszeit') as HTMLInputElement).value,
          (document.getElementById('Notizen') as HTMLInputElement).value);

      if (!this.create && this.automated !== -1) {
        tmp.einsatzStatusID = this.selectedStatus;
        if ((document.getElementById('startzeit') as HTMLInputElement).value !== '') {
          tmp.startzeit = (document.getElementById('startzeit') as HTMLInputElement).value;
        } else {
          if (this.data.einsatzStatusID === 1 && tmp.einsatzStatusID === 2) {
            const d = new Date(Date.now());
            tmp.startzeit = formatDate(d);
          }
        }
        if ((document.getElementById('endzeit') as HTMLInputElement).value !== '') {
          tmp.endzeit = (document.getElementById('endzeit') as HTMLInputElement).value;
          this.finishEinsatz(tmp.einsatznummer);
        } else {
          if (tmp.einsatzStatusID === 3) {
            const d = new Date(Date.now());
            tmp.endzeit = formatDate(d);
            this.finishEinsatz(tmp.einsatznummer);
          }
        }
        if (tmp.einsatzStatusID === 1 && this.data.einsatzStatusID === 3) {
          tmp.startzeit = null;
          tmp.endzeit = null;
        }
        else {
          if (tmp.einsatzStatusID === 2 && this.data.einsatzStatusID === 3) {
            tmp.endzeit = null;
          } else {
            if (tmp.endzeit != null && tmp.einsatzStatusID < 3) {
              tmp.einsatzStatusID = 3;
              this.finishEinsatz(tmp.einsatznummer);
            }
          }
          if (tmp.einsatzStatusID === 1 && this.data.einsatzStatusID === 2) {
            tmp.startzeit = null;
            tmp.endzeit = null;
          } else {
            if (tmp.startzeit != null && tmp.einsatzStatusID === 1) {
              tmp.einsatzStatusID = 2;
            }
          }
        }
      }
      this.dialog.close(tmp);
    }
    else {
      openErrorDialog(checkData[1], this.delLog);
    }
  }

  checkRequired(): [found: boolean, errString: string] {
    let errStr = 'Die Felder:';
    let retVal = true;
    for (const str of this.requiredData) {
      if ((document.getElementById(str) as HTMLInputElement).value === '') {
        retVal = false;
        errStr += (' ' + str + ',');

      }
    }
    errStr = errStr.slice(0, errStr.length - 1);
    errStr += ' sind Pflichtfelder!';

    return [retVal, errStr];
  }

  delBehavior = (toDel: boolean) => {
    if (toDel) {
      if (this.data.einsatzStatusID === 2) {
        openErrorDialog('Bitte schließen Sie den Einsatz zuvor ab und bestätigen Sie diese Aktion!', this.delLog);
      } else {
        this.dialog.close(this.data.einsatznummer);
      }
    }
  }

  delete() {
    this.dataService.deleteDialogue(this.delBehavior, this.delLog);
  }

  nuesslerSelected() {
    this.dialog.close();
    const theChosenOne: any = this.nuesslerList[this.selectedNuessler];
    const subData = this.data.PLZ + '|' + this.data.ortstadt + '|' + this.data.strasse + '|' + this.data.hausnummer + '|' + theChosenOne.selection + '|' + this.data.einsatznummer + '|' + theChosenOne.tiefkalt + '|' + theChosenOne.vollbrand + '|' + theChosenOne.menge + '|' + theChosenOne.unNummer + '|' + 0;
    this.router.navigate(['result-page'], {state: {data: subData}});
  }

  finishEinsatz(nr: number) {
    this.http.get(environment.serverUrl + `getEinsatzmittelzuteilung/${nr}`).subscribe((res: any) => {
      if (res.found) {
        for (const ele of res.retVal) {
          const body1 = {
            aktuelleInsassen: 0,
            zugeteilt: 0,
            einsatzmittelID: ele.einsatzmittelID,
            einsatzStatusID: 2
          };
          this.http.put(environment.serverUrl + `putAktuelleinsassenZugeteilt`, body1).subscribe(() => {
            const body2 = {
              einzuID: ele.einsatzmittelzuteilungID,
              endZeit: new Date(Date.now())
            };
            this.http.put(environment.serverUrl + `putEinsatzmittelzuteilungEndzeit`, body2).subscribe(() => {});
          });
        }
      }
    });
  }

}
