import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {Einsatzmittel} from '../../../POJOs/Einsatzmittel';
import {MatTableDataSource} from '@angular/material/table';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-add-fzg-zu-einsatz-dialog',
  templateUrl: './add-fzg-zu-einsatz-dialog.component.html',
  styleUrls: ['./add-fzg-zu-einsatz-dialog.component.scss']
})
export class AddFzgZuEinsatzDialogComponent implements OnInit {

  einsatzMittel: Einsatzmittel[] = [];
  dataSource = new MatTableDataSource(this.einsatzMittel);
  displayedColumns: string[] = ['kurzbezeichnung', 'decreaseInsassen', 'aktuelleInsassen', 'increaseInsassen', 'feuerwehr'];
  maxReached = false;
  minReached = false;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient, public dialog: MatDialogRef<AddFzgZuEinsatzDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.http.get('http://localhost:5000/getEinsatzmittelUndFeuerwehr').subscribe((einsatzmittel: any) => {
      for (const einsatzmittelData of einsatzmittel.retVal) {
        if ((einsatzmittelData.einsatzmittelStatusID === 1 || einsatzmittelData.einsatzmittelStatusID === 2) && einsatzmittelData.zugeteilt !== 1) {
          einsatzmittelData.minReached = einsatzmittelData.aktuelleInsassen === 0;
          einsatzmittelData.maxReached = einsatzmittelData.aktuelleInsassen === einsatzmittelData.insassen;
          this.einsatzMittel.push(einsatzmittelData);
        }
      }
      this.dataSource.data = this.einsatzMittel;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (err) => console.log(err));
  }

  increaseInsassen(elem) {
    if (elem.aktuelleInsassen === elem.insassen) {
      elem.maxReached = true;
      elem.minReached = false;
    } else {
      elem.maxReached = false;
      elem.minReached = false;
      elem.aktuelleInsassen++;
    }
  }

  decreaseInsassen(elem) {
    if (elem.aktuelleInsassen <= 0) {
      elem.minReached = true;
      elem.maxReached = false;
    } else {
      elem.minReached = false;
      elem.maxReached = false;
      elem.aktuelleInsassen--;
    }
  }

  update() {
    for (const f of this.einsatzMittel) {
      if (f.aktuelleInsassen > 0) {
        const res = {
          einsatzmittelID: f.einsatzmittelID,
          einsatznummer: this.data.einsatznummer,
          zuteilungvon: new Date(Date.now())
        };
        this.http.post('http://localhost:5000/postEinsatzmittelzuteilung', res).subscribe(() => {
          const res1 = {
            aktuelleInsassen: f.aktuelleInsassen,
            zugeteilt: true,
            einsatzmittelID: f.einsatzmittelID,
            einsatzStatusID: 1
          };
          this.http.put('http://localhost:5000/putAktuelleinsassenZugeteilt', res1).subscribe(() => {
            if (this.data.einsatzStatusID === 1) {
              const res2 = {
                startZeit: new Date(Date.now()),
                einsatzStatusID: 2,
                einsatznummer: this.data.einsatznummer
              };
              this.http.put('http://localhost:5000/putStatusStartZeit', res2).subscribe(() => {
                this.dialog.close();
              }, (err) => console.log(err));
            }
            this.dialog.close();
          }, (err) => console.log(err));
        }, (err) => console.log(err));
      }
    }
  }
}









