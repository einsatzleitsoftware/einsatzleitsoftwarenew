import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFzgZuEinsatzDialogComponent } from './add-fzg-zu-einsatz-dialog.component';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {MAT_DIALOG_DATA, MAT_DIALOG_SCROLL_STRATEGY, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Overlay} from '@angular/cdk/overlay';
import {MatCard, MatCardContent} from '@angular/material/card';
import {Einsatzmittel} from '../../../POJOs/Einsatzmittel';

describe('AddFzgZuEinsatzDialogComponent', () => {
  let component: AddFzgZuEinsatzDialogComponent;
  let fixture: ComponentFixture<AddFzgZuEinsatzDialogComponent>;
  const einsatzmittel = new Einsatzmittel(999999, 'Rüstlöschfahrtezg', 'RLF', 7, 2, 1);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFzgZuEinsatzDialogComponent ],
      providers: [HttpClient, HttpHandler, MatDialog, Overlay, {provide: MAT_DIALOG_DATA, useValue: {}},
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_SCROLL_STRATEGY, useValue: {}}, MatCard, MatCardContent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFzgZuEinsatzDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should increase insassen', () => {
    component.increaseInsassen(einsatzmittel);
    expect(einsatzmittel.aktuelleInsassen).toBe(1);
  });
  it('should decrease insassen', () => {
    component.decreaseInsassen(einsatzmittel);
    expect(einsatzmittel.aktuelleInsassen).toBe(0);
  });
});
