import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-loesch-dialog',
  templateUrl: './loesch-dialog.component.html',
  styleUrls: ['./loesch-dialog.component.scss']
})
export class LoeschDialogComponent implements OnInit {

  constructor(public dialog: MatDialogRef<LoeschDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {
    text: string,
    title: string,
    callback: any
  }) { }

  ngOnInit(): void {
  }

  cancel() {
    if(this.data.callback !== undefined)
      this.data.callback(false);
    this.dialog.close(false);
  }

  delete() {
    if(this.data.callback !== undefined)
      this.data.callback(true);
    this.dialog.close(true);
  }
}
