import {ErrorDialogComponent} from '../app/dialogues/error-dialog/error-dialog.component';
import {MatDialog} from '@angular/material/dialog';

export function openErrorDialog(errText: string, dialog: MatDialog) {
  dialog.open(ErrorDialogComponent, {
    width: '650px',
    height: '250px',
    data: errText,
    disableClose: true,
  });
}
