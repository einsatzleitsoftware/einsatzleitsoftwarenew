import {Selection} from '../POJOs/Selection';
import {Input} from '../POJOs/Input';
import {Radius} from '../POJOs/Radius';
import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';


export class NuesslerBerechnung {
  // tslint:disable-next-line:variable-name
  private _berechnung = '';

  getClass(sel: Selection): string {
    if (sel.nebel) {
      if (sel.brand) {
        return 'e';
      } else {
        return 'f';
      }
    } else {
      if (sel.starkWind) {
        if (sel.brand) {
          return 'c';
        } else {
          return 'd';
        }
      } else {
        if (sel.bedeckt) {
          if (sel.brand) {
            return 'c';
          } else {
            return 'd';
          }
        } else {
          if (!sel.tag) {
            if (sel.leichtWind) {
              if (sel.brand) {
                return 'd';
              } else {
                return 'e';
              }
            } else {
              if (sel.brand) {
                return 'e';
              } else {
                return 'f';
              }
            }
          } else {
            if (sel.sehrLeichtWind) {
              if (sel.fruehlingSommer) {
                if (sel.brand) {
                  return 'a';
                } else {
                  return 'b';
                }
              } else {
                if (sel.brand) {
                  return 'b';
                } else {
                  return 'c';
                }
              }
            }
          }
        }
      }
    }
  }

  calcStellen(d: number): number {
    let s: string = d.toString();
    s = s.substring(s.indexOf('.') + 1);
    if (s.charAt(0) !== '0') {
      return 1;
    }
    let retVal = 2;
    for (let i = 1; i < s.length; i++) {
      if (s.charAt(i) === '0') {
        retVal++;
      } else {
        break;
      }
    }
    return retVal;
  }

  calcPAC(wert: number): number[] {
    const retVal: number[] = [];
    let zwert: number = wert;
    if (wert > 1) {
      zwert = Math.floor(wert);
    }

    let anz: number = wert / 10.0;
    if (wert > 4000) {
      return [4000, -1];
    }
    if (wert < 0.08) {
      return [0.08, -1];
    }
    if (anz >= 0.1 && zwert >= 1) {
      if (anz < 0.8) {
        anz = 0;
      } else {
        const floor = Math.floor(anz);
        anz = String(floor.valueOf()).length;
      }
    } else {
      anz = this.calcStellen(wert) * -1;
    }

    const hoch: number = Math.pow(10, anz);

    if (wert < 4 * hoch) {
      retVal[0] = 8 * (hoch / 10);
      retVal[1] = 4 * hoch;
      return retVal;
    }
    if (wert > 4 * hoch) {
      retVal[0] = 4 * hoch;
      retVal[1] = 8 * hoch;
      return retVal;
    }

    retVal[0] = wert;
    retVal[1] = wert;
    return retVal;

  }


  calcMenge(wert: number): number[] {
    const retVal: number[] = [];
    let anz: number = wert / 10;

    if (wert > 50000) {
      return [50000, -1];
    }
    if (wert < 10) {
      return [10, -1];
    }

    anz = String(Math.floor(anz.valueOf())).length;
    const hoch: number = Math.pow(10, anz);
    if (wert > 1 * hoch && wert < 2 * hoch) {
      retVal[0] = 1 * hoch;
      retVal[1] = 2 * hoch;
      return retVal;
    }
    if (wert > 2 * hoch && wert < 5 * hoch) {
      retVal[0] = 2 * hoch;
      retVal[1] = 5 * hoch;
      return retVal;
    }
    if (wert > 5 * hoch && wert < 10 * hoch) {
      retVal[0] = 5 * hoch;
      retVal[1] = 1 * hoch * 10;
      return retVal;
    }
    retVal[0] = wert;
    retVal[1] = wert;
    return retVal;
  }

  getValue(w1: number, w2: number, kl: number, gr: number, eingabe: number) {
    let retVal: number;
    retVal = w2 - w1;
    retVal = (retVal / (gr - kl));
    retVal = retVal * (eingabe - kl);
    return retVal + w1;
  }

  specialCase(cl: string, inp: Input) {
    let ascii: number;
    if (inp.tiefkalt) {
      ascii = cl.charCodeAt(0);
      ascii++;
      cl = String.fromCharCode(ascii);
    }
    if (inp.vollbrand) {
      if (!(cl === 'a')) {
        ascii = cl.charCodeAt(0);
        ascii--;
        cl = String.fromCharCode(ascii);
      }
    }
    return cl;
  }

  // @ts-ignore
  async finalValue(klass: string, pac: number[], menge: number[], input: Input, pacEcht: number, massFactor: number): Promise<Radius> {
    let retVal;
    let mengeGleichTxt: string = ' ist genau eine Richtlinie in der Tabelle. \n';
    let pacGleichTxt: string = ' ist genau eine Richtlinie in der Tabelle. \n';
    let spezFall: boolean = false;

    if (pac[1] == -1.0) {
      pacGleichTxt = ' ist gr&ouml;&szlig;er/kleiner als in der Tabelle vorgesehen.<br>Es wird daher der n&auml;here Wert angenommen.<br>';
      pac[1] = pac[0];
      spezFall = true;
    }

    if (menge[1] == -1.0) {
      mengeGleichTxt = ' ist größer/kleiner als in der Tabelle vorgesehen.\nEs wird daher der nähere Wert angenommen.\n';
      menge[1] = menge[0];
      spezFall = true;
    }

    this.berechnung += '\nDie Menge ' + input.menge + 'kg ist unter Berücksichtigung des MassFactors von ' + massFactor + ': ';
    this.berechnung += input.menge * massFactor + 'kg \n';

    if (pac[0] == pac[1] && menge[0] == menge[1]) {
      if (spezFall == false) {
        this.berechnung += 'Der Wert ist genau auf den Tabellenwerten: PAC=' + pac[0] + ' Menge=' + menge[0] + '\n';
      } else {
        this.berechnung += 'Ein Wert ist größer/kleiner als in der Tabelle vorgesehen.\nEs wird daher der nähere Wert angenommen.\n';
      }
      this.berechnung = this.berechnung + 'Das Ergebnis ist daher:\n';
      this.getRadius(klass, pac[1], Number(menge[0]))
        .then(
          (response) => {
            let end: Radius = JSON.parse(response.data).retVal;
            this.berechnung = this.berechnung + 'Ausbreitungsradius im Freien: ' + Math.round(end.werta) + 'm\nAusbreitungsradius im Inneren: ' + Math.round(end.werti) + 'm';
            retVal = end;
            //return Promise.resolve(end);
          });
    }

    let w1: Radius = new Radius();
    let w2: Radius = new Radius();
    let w3: Radius = new Radius();
    let w4: Radius = new Radius();
    await this.getRadius(klass, pac[1], Number(menge[0])).then(async (response) => {
      w1 = response.data.retVal;
      await this.getRadius(klass, pac[1], Number(menge[1])).then( async (response1) => {
        w2 = response1.data.retVal;
        await this.getRadius(klass, pac[0], Number(menge[0])).then(async (response2) => {
          w3 = response2.data.retVal;
          await this.getRadius(klass, pac[0], Number(menge[1])).then(async (response3) => {
            w4 = response3.data.retVal;


            let zw1: Radius = new Radius();
            let zw2: Radius = new Radius();

            if (menge[0] != menge[1]) {
              this.berechnung += '\nDie Menge ' + input.menge * massFactor + 'kg liegt zwischen den Mengenwerten: ' + menge[0] + 'kg und ' + menge[1] + 'kg\n';
              this.berechnung += 'Die Differenz dieser Mengen ist ' + (menge[1] - menge[0]) + 'kg';
              zw1.werta = (this.getValue(w1.werta, w2.werta, menge[0], menge[1], input.menge * massFactor));
              zw1.werti = (this.getValue(w1.werti, w2.werti, menge[0], menge[1], input.menge * massFactor));
              zw2.werta = (this.getValue(w3.werta, w4.werta, menge[0], menge[1], input.menge * massFactor));
              zw2.werti = (this.getValue(w3.werti, w4.werti, menge[0], menge[1], input.menge * massFactor));

              if (pac[0] != pac[1]) {
                this.berechnung += '\nDiese Differenz der Menge hat für den PAC ' + pac[0] + ' eine Ausbreitung von: ' + '\nTabelle Außen: ' + Math.round(w4.werta - w3.werta) + 'm\nTabelle Innen: ' + Math.round(w4.werti - w3.werti) + 'm';
                this.berechnung += '\nDas heißt für eine Menge von ' + Math.round(input.menge) + 'kg und dem PAC von ' + pac[0] + ' ist die Ausbreitung:\nTabelle Außen: ' + Math.round(zw2.werta) + 'm\nTabelle Innen: ' + Math.round(zw2.werti) + 'm';

                this.berechnung += '\nDiese Differenz der Menge hat für den PAC ' + pac[1] + ' eine Absprerrung von: ' + '\nTabelle Außen: ' + Math.round(w2.werta - w1.werta) + 'm\nTabelle Innen: ' + Math.round(w2.werti - w1.werti) + 'm';
                this.berechnung += '\nDas heißt für eine Menge von ' + Math.round(input.menge) + 'kg und dem PAC von ' + pac[1] + ' ist die Ausbreitung\nTabelle Außen: ' + Math.round(zw1.werta) + 'm\nTabelle Innen: ' + Math.round(zw1.werti) + 'm';
              } else {
                this.berechnung += '\nDiese Differenz der Menge hat' + ' eine Ausbreitung von: ' + '\nTabelle Außen: ' + Math.round(w2.werta - w1.werta) + 'm\nTabelle Innen: ' + Math.round(w2.werti - w1.werti) + 'm';
                this.berechnung += '\nDas heißt für eine Menge von ' + Math.round(input.menge) + 'kg  ist die Ausbreitung:\nTabelle Außen: ' + Math.round(zw1.werta) + 'm\nTabelle Innen: ' + Math.round(zw1.werti) + 'm';
              }

            } else {
              this.berechnung = this.berechnung + '\nDie Menge ' + Math.round(input.menge) + 'kg' + mengeGleichTxt;
              zw1 = w1;
              zw2 = w3;
            }

            if (pac[0] === pac[1]) {
              this.berechnung = this.berechnung + '\nDer PAC-Wert ' + pacEcht + pacGleichTxt;
              this.berechnung = this.berechnung + 'Es ergibt sich folgendes Ergebnis, da ' + Math.round(input.menge) + ' um ' + Math.round(input.menge - menge[0]) + ' größer ist als der kleinere Wert auf der Tabelle.\n';
              this.berechnung = this.berechnung + 'Ausbreitungsradius im Freien: ' + Math.round(zw1.werta) + 'm\nAusbreitungsradius im Inneren: ' + Math.round(zw2.werti) + 'm';
              retVal = zw1;
              //return Promise.resolve(zw1);
            } else {
              const erg: Radius = new Radius();
              erg.werta = (Math.round(zw2.werta - (this.getValue(zw1.werta, zw2.werta, pac[0], pac[1], pacEcht) - zw1.werta)));
              erg.werti = (Math.round(zw2.werti - (this.getValue(zw1.werti, zw2.werti, pac[0], pac[1], pacEcht) - zw1.werti)));

              this.berechnung = this.berechnung + '\nEs ergeben sich folgende Zwischenwerte, da ' + Math.round(input.menge) + 'kg um ' + Math.round(input.menge - menge[0]) + 'kg größer ist als der kleinere Wert auf der Tabelle.\n';
              this.berechnung = this.berechnung + 'Zwischenwert 1: Innen: ' + Math.round(zw1.werti) + 'm Außen: ' + Math.round(zw1.werta) + 'm\n';
              this.berechnung = this.berechnung + 'Zwischenwert 2: Innen: ' + Math.round(zw2.werti) + 'm Außen: ' + Math.round(zw2.werta) + 'm\n';
              this.berechnung = this.berechnung + 'Der PAC ' + pacEcht + ' liegt zwischen den Werten: ' + pac[0] + ' und ' + pac[1] + '\n';
              this.berechnung = this.berechnung + 'Die Differenz der Tabellenwerte beträgt ' + (pac[1] - pac[0]);
              //Interpolation
              this.berechnung += '\nDiese Differenz hat' + ' eine Ausbreitung von: ' + '\nTabelle Außen: ' + Math.round(zw2.werta - zw1.werta) + 'm\nTabelle Innen: ' + Math.round(zw2.werti - zw1.werti) + 'm';
              this.berechnung += '\nDas heißt für einen PAC-Wert von ' + pacEcht + ' ist die Ausbreitung:\nTabelle Außen: ' + Math.round(erg.werta) + 'm\nTabelle Innen: ' + Math.round(erg.werti) + 'm';

              this.berechnung = this.berechnung + '\nDaher ist das Ergebnis:\n';
              this.berechnung = this.berechnung + 'Ausbreitungsradius im Freien: ' + Math.round(erg.werta) + 'm\nAusbreitungsradius im Inneren: ' + Math.round(erg.werti) + 'm';
              retVal = erg;
              // return Promise.resolve(erg);
            }
          });
        });
      });
    });
    return retVal;
  }

  getRadius(kl, pac, menge) {
    return axios.get(`http://localhost:5000/getRadius?kl=${kl.toLowerCase()}&pac=${pac}&menge=${menge}`);
  }


  get berechnung(): string {
    return this._berechnung;
  }

  set berechnung(value: string) {
    this._berechnung = value;
  }
}

