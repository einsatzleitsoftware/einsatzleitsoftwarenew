export function formatDate(d: Date): string {
  let retVal = '';
  retVal += d.getFullYear();
  retVal += '-';
  (d.getMonth() + 1) < 10 ? retVal += '0' + (d.getMonth() + 1) : retVal += (d.getMonth() + 1);
  retVal += '-';
  d.getDate() < 10 ? retVal += '0' + d.getDate() : retVal += d.getDate();
  retVal += 'T';
  d.getHours() < 10 ? retVal += '0' + d.getHours() : retVal += d.getHours();
  retVal += ':';
  d.getMinutes() < 10 ? retVal += '0' + d.getMinutes() : retVal += d.getMinutes();

  return retVal;
}

export function dateString(datum: Date) {
  let retVal: string;
  retVal = datum.getDate() + '.' + (datum.getMonth() + 1) + '.' + datum.getFullYear() + ' ';
  datum.getHours() < 10 ? retVal += ('0' + datum.getHours()) : retVal += datum.getHours();
  retVal += ':';
  datum.getMinutes() < 10 ? retVal += ('0' + datum.getMinutes()) : retVal += datum.getMinutes();
  return retVal;
}

