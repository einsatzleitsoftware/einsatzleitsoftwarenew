import {Injectable} from '@angular/core';

@Injectable()
export class Einsatz {
  private _einsatznummer: number;
  private _einsatzstichwort: string;
  private _notfallgeschehen: string;
  private _sachverhalt: string;
  private _alarmierendesEinsatzmittel: string;
  private _objekt: string;
  private _strasse: string;
  private _hausnummer: number;
  private _PLZ: number;
  private _ortstadt: string;
  private _einsatzStatusID: number;
  private _LSZAlarmierung: boolean;
  private _eingangszeit: string;
  private _startzeit: string;
  private _endzeit: string;
  private _lat: number;
  private _lng: number;
  private _notizen: string;

  constructor(einsatznummer?: number, einsatzstichwort?: string, notfallgeschehen?: string, sachverhalt?: string, alarmierendesEinsatzmittel?: string,  objekt?: string, strass?: string,
              hausnummer?: number, PLZ?: number, ortstadt?: string,  einsatzStatusID?: number, LSZAlarmierung?: boolean, eingangszeit?: string, notizen?: string) {
    this._einsatznummer = einsatznummer;
    this._einsatzstichwort = einsatzstichwort;
    this._notfallgeschehen = notfallgeschehen;
    this._sachverhalt = sachverhalt;
    this._alarmierendesEinsatzmittel = alarmierendesEinsatzmittel;
    this._objekt = objekt;
    this._strasse = strass;
    this._hausnummer = hausnummer;
    this._PLZ = PLZ;
    this._ortstadt = ortstadt;
    this._einsatzStatusID = einsatzStatusID;
    this._LSZAlarmierung = LSZAlarmierung;
    this._eingangszeit = eingangszeit;
    this._startzeit = null;
    this._endzeit = null;
    this._notizen = notizen;
  }

  get einsatznummer(): number {
    return this._einsatznummer;
  }

  set einsatznummer(value: number) {
    this._einsatznummer = value;
  }

  get einsatzstichwort(): string {
    return this._einsatzstichwort;
  }

  set einsatzstichwort(value: string) {
    this._einsatzstichwort = value;
  }

  get notfallgeschehen(): string {
    return this._notfallgeschehen;
  }

  set notfallgeschehen(value: string) {
    this._notfallgeschehen = value;
  }

  get sachverhalt(): string {
    return this._sachverhalt;
  }

  set sachverhalt(value: string) {
    this._sachverhalt = value;
  }

  get alarmierendesEinsatzmittel(): string {
    return this._alarmierendesEinsatzmittel;
  }

  set alarmierendesEinsatzmittel(value: string) {
    this._alarmierendesEinsatzmittel = value;
  }

  get objekt(): string {
    return this._objekt;
  }

  set objekt(value: string) {
    this._objekt = value;
  }

  get strasse(): string {
    return this._strasse;
  }

  set strasse(value: string) {
    this._strasse = value;
  }

  get hausnummer(): number {
    return this._hausnummer;
  }

  set hausnummer(value: number) {
    this._hausnummer = value;
  }

  get PLZ(): number {
    return this._PLZ;
  }

  set PLZ(value: number) {
    this._PLZ = value;
  }

  get ortstadt(): string {
    return this._ortstadt;
  }

  set ortstadt(value: string) {
    this._ortstadt = value;
  }

  get einsatzStatusID(): number {
    return this._einsatzStatusID;
  }

  set einsatzStatusID(value: number) {
    this._einsatzStatusID = value;
  }

  get LSZAlarmierung(): boolean {
    return this._LSZAlarmierung;
  }

  set LSZAlarmierung(value: boolean) {
    this._LSZAlarmierung = value;
  }

  get eingangszeit(): string {
    return this._eingangszeit;
  }

  set eingangszeit(value: string) {
    this._eingangszeit = value;
  }

  get startzeit(): string {
    return this._startzeit;
  }

  set startzeit(value: string) {
    this._startzeit = value;
  }

  get endzeit(): string {
    return this._endzeit;
  }

  set endzeit(value: string) {
    this._endzeit = value;
  }

  get lat(): number {
    return this._lat;
  }

  set lat(value: number) {
    this._lat = value;
  }

  get lng(): number {
    return this._lng;
  }

  set lng(value: number) {
    this._lng = value;
  }

  get notizen(): string {
    return this._notizen;
  }

  set notizen(value: string) {
    this._notizen = value;
  }
}
