import {Injectable} from '@angular/core';

@Injectable()
export class Selection{
  public _nebel: boolean;
  public _starkWind: boolean;
  public _bedeckt: boolean;
  public _tag: boolean;
  public _leichtWind: boolean;
  public _sehrLeichtWind: boolean;
  public _fruehlingSommer: boolean;
  public _brand: boolean;


  get nebel(): boolean {
    return this._nebel;
  }

  set nebel(value: boolean) {
    this._nebel = value;
  }

  get starkWind(): boolean {
    return this._starkWind;
  }

  set starkWind(value: boolean) {
    this._starkWind = value;
  }

  get bedeckt(): boolean {
    return this._bedeckt;
  }

  set bedeckt(value: boolean) {
    this._bedeckt = value;
  }

  get tag(): boolean {
    return this._tag;
  }

  set tag(value: boolean) {
    this._tag = value;
  }

  get leichtWind(): boolean {
    return this._leichtWind;
  }

  set leichtWind(value: boolean) {
    this._leichtWind = value;
  }

  get sehrLeichtWind(): boolean {
    return this._sehrLeichtWind;
  }

  set sehrLeichtWind(value: boolean) {
    this._sehrLeichtWind = value;
  }

  get fruehlingSommer(): boolean {
    return this._fruehlingSommer;
  }

  set fruehlingSommer(value: boolean) {
    this._fruehlingSommer = value;
  }

  get brand(): boolean {
    return this._brand;
  }

  set brand(value: boolean) {
    this._brand = value;
  }
}
