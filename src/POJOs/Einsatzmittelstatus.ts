import {Injectable} from '@angular/core';

@Injectable()
export class Einsatzmittelstatus {
  private _einsatzmittelStatusID: number;
  private _status: string;

  get einsatzStatusID(): number {
    return this._einsatzmittelStatusID;
  }

  set einsatzStatusID(value: number) {
    this._einsatzmittelStatusID = value;
  }

  get status(): string {
    return this._status;
  }

  set status(value: string) {
    this._status = value;
  }
}
