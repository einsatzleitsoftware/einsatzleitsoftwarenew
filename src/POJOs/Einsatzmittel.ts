import {Injectable} from '@angular/core';

@Injectable()
export class Einsatzmittel {
  private _einsatzmittelID: number;
  private _einsatzmittelName: string;
  private _kurzbezeichnung: string;
  private _einsatzmittelStatusID: number;
  private _feuerwehrID: number;
  private _insassen: number;
  private _aktuelleInsassen: number;
  private _zugeteilt: boolean;
  private _notizen: string;

  constructor( einsatzmittelID: number, einsatzmittelName: string, kurzbezeichnung: string, insassen: number, einsatzmittelStatusID: number, feuerwehrID: number, notizen?: string) {
    this._einsatzmittelID = einsatzmittelID;
    this._einsatzmittelName = einsatzmittelName;
    this._kurzbezeichnung = kurzbezeichnung;
    this._einsatzmittelStatusID = einsatzmittelStatusID;
    this._feuerwehrID = feuerwehrID;
    this._insassen = insassen;
    this._aktuelleInsassen = 0;
    this._zugeteilt = false;
    this._notizen = notizen;
  }

  get einsatzmittelStatusID(): number {
    return this._einsatzmittelStatusID;
  }

  set einsatzmittelStatusID(value: number) {
    this._einsatzmittelStatusID = value;
  }

  get einsatzmittelID(): number {
    return this._einsatzmittelID;
  }

  set einsatzmittelID(value: number) {
    this._einsatzmittelID = value;
  }

  get einsatzmittelName(): string {
    return this._einsatzmittelName;
  }

  set einsatzmittelName(value: string) {
    this._einsatzmittelName = value;
  }

  get kurzbezeichnung(): string {
    return this._kurzbezeichnung;
  }

  set kurzbezeichnung(value: string) {
    this._kurzbezeichnung = value;
  }


  get feuerwehrID(): number {
    return this._feuerwehrID;
  }

  set feuerwehrID(value: number) {
    this._feuerwehrID = value;
  }

  get insassen(): number {
    return this._insassen;
  }

  set insassen(value: number) {
    this._insassen = value;
  }

  get zugeteilt(): boolean {
    return this._zugeteilt;
  }

  set zugeteilt(value: boolean) {
    this._zugeteilt = value;
  }

  get aktuelleInsassen(): number {
    return this._aktuelleInsassen;
  }

  set aktuelleInsassen(value: number) {
    this._aktuelleInsassen = value;
  }

  get notizen(): string {
    return this._notizen;
  }

  set notizen(value: string) {
    this._notizen = value;
  }
}
