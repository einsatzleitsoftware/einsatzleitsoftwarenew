import { TestBed } from '@angular/core/testing';
import {Feuerwehr} from './Feuerwehr';

describe('Feuerwehr', () => {
  let feuerwehr: Feuerwehr;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        Feuerwehr
      ],
    }).compileComponents();
    feuerwehr = new Feuerwehr(9999, 'Tschanigraben', 2);
  });

  it('TestFeuerwehrID', () => {
    feuerwehr.feuerwehrID = 2;
    expect(feuerwehr.feuerwehrID).toBe(2);
  });

  it('TestOrt', () => {
    feuerwehr.ort = 'Rudersdorf';
    expect(feuerwehr.ort).toBe('Rudersdorf');
  });

  it('TestFeuerwehrStatusID', () => {
    feuerwehr.feuerwehrStatusID = 1;
    expect(feuerwehr.feuerwehrStatusID).toBe(1);
  });
});
