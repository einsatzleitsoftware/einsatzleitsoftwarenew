import { TestBed } from '@angular/core/testing';
import {Einsatzmittel} from './Einsatzmittel';

describe('Einsatzmittel', () => {
  let einsatzmittel: Einsatzmittel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        Einsatzmittel
      ],
    }).compileComponents();
    einsatzmittel = new Einsatzmittel(200, 'Kleinlöschfahrzeug', 'KLF', 3, 2, 2);
  });

  it('TestEinsatzmittelID', () => {
    einsatzmittel.einsatzmittelID = 300;
    expect(einsatzmittel.einsatzmittelID).toBe(300);
  });

  it('TestEinsatzmittelName', () => {
    einsatzmittel.einsatzmittelName = 'Tanklöschfahrzeug';
    expect(einsatzmittel.einsatzmittelName).toBe('Tanklöschfahrzeug');
  });

  it('TestKurzbezeichnung', () => {
    einsatzmittel.kurzbezeichnung = 'TLF';
    expect(einsatzmittel.kurzbezeichnung).toBe('TLF');
  });

  it('TestEinsatzmittelStatusID', () => {
    einsatzmittel.einsatzmittelStatusID = 1;
    expect(einsatzmittel.einsatzmittelStatusID).toBe(1);
  });

  it('TestInsassen', () => {
    einsatzmittel.insassen = 2;
    expect(einsatzmittel.insassen).toBe(2);
  });

  it('TestFeuerwehrID', () => {
    einsatzmittel.feuerwehrID = 2;
    expect(einsatzmittel.feuerwehrID).toBe(2);
  });

  it('TestAktuelleInsassen', () => {
    einsatzmittel.aktuelleInsassen = 1;
    expect(einsatzmittel.aktuelleInsassen).toBe(1);
  });

  it('TestZugeteilt', () => {
    einsatzmittel.zugeteilt = true;
    expect(einsatzmittel.zugeteilt).toBe(true);
  });
});
