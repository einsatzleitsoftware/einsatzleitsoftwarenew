import { TestBed } from '@angular/core/testing';
import {Einsatz} from "./Einsatz";

describe('Einsatz', () => {
  let einsatz: Einsatz;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        Einsatz
      ],
    }).compileComponents();
    einsatz = new Einsatz();
  });

  it('TestEinsatzstichwort', () => {
    einsatz.einsatzstichwort = 'T3';
    expect(einsatz.einsatzstichwort).toBe('T3');
  });

  it('TestEinsatznummer', () => {
    einsatz.einsatznummer = 12345;
    expect(einsatz.einsatznummer).toBe(12345);
  });

  it('TestNotfallgeschehen', () => {
    einsatz.notfallgeschehen = 'Autounfall';
    expect(einsatz.notfallgeschehen).toBe('Autounfall');
  });

  it('TestSachverhalt', () => {
    einsatz.sachverhalt = 'Auto gegen Baum';
    expect(einsatz.sachverhalt).toBe('Auto gegen Baum');
  });

  it('TestAlarmEM', () => {
    einsatz.alarmierendesEinsatzmittel = 'Sirene';
    expect(einsatz.alarmierendesEinsatzmittel).toBe('Sirene');
  });

  it('TestObjekt', () => {
    einsatz.objekt = 'Auto';
    expect(einsatz.objekt).toBe('Auto');
  });

  it('TestStrasse', () => {
    einsatz.strasse = 'Marbachstrasse';
    expect(einsatz.strasse).toBe('Marbachstrasse');
  });

  it('TestHausnummer', () => {
    einsatz.hausnummer = 39;
    expect(einsatz.hausnummer).toBe(39);
  });

  it('TestPLZ', () => {
    einsatz.PLZ = 7571;
    expect(einsatz.PLZ).toBe(7571);
  });

  it('TestOrtStadt', () => {
    einsatz.ortstadt = 'Rudersdorf';
    expect(einsatz.ortstadt).toBe('Rudersdorf');
  });

  it('TestEinsatzstatusID', () => {
    einsatz.einsatzStatusID = 2;
    expect(einsatz.einsatzStatusID).toBe(2);
  });

  it('TestLSZ', () => {
    einsatz.LSZAlarmierung = true;
    expect(einsatz.LSZAlarmierung).toBeTrue();
  });

  it('TestEingangszeit', () => {
    einsatz.eingangszeit = '2021-03-02T12:55:23';
    expect(einsatz.eingangszeit).toBe('2021-03-02T12:55:23');
  });

  it('TestStartzeit', () => {
    einsatz.startzeit = '2021-03-02T12:55:23';
    expect(einsatz.startzeit).toBe('2021-03-02T12:55:23');
  });

  it('TestEndzeit', () => {
    einsatz.endzeit = '2021-03-02T12:55:23';
    expect(einsatz.endzeit).toBe('2021-03-02T12:55:23');
  });

  it('TestLat', () => {
    einsatz.lat = 47.065819853069094;
    expect(einsatz.lat).toBe(47.065819853069094);
  });

  it('TestLng', () => {
    einsatz.lng = 16.139600467072324;
    expect(einsatz.lng).toBe(16.139600467072324);
  });
});
