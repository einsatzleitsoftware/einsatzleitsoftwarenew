import {Injectable} from '@angular/core';
import {Selection} from './Selection';

@Injectable()
export class Input {
  public _selection: Selection;
  _unNumb: number = null;
  public _menge: number = null;
  public _postleitzahl: string;
  public _ort: string;
  public _strasse: string;
  public _hausnr: string;
  public _tiefkalt = false;
  public _vollbrand = false;

  constructor() {
    this._selection = new Selection();
  }


  get selection() {
    return this._selection;
  }

  set selection(value) {
    this._selection = value;
  }

  get unNumb(): number {
    return this._unNumb;
  }

  set unNumb(value: number) {
    this._unNumb = value;
  }

  get menge(): number {
    return this._menge;
  }

  set menge(value: number) {
    this._menge = value;
  }

  get postleitzahl(): string {
    return this._postleitzahl;
  }

  set postleitzahl(value: string) {
    this._postleitzahl = value;
  }

  get ort(): string {
    return this._ort;
  }

  set ort(value: string) {
    this._ort = value;
  }

  get strasse(): string {
    return this._strasse;
  }

  set strasse(value: string) {
    this._strasse = value;
  }

  get hausnr(): string {
    return this._hausnr;
  }

  set hausnr(value: string) {
    this._hausnr = value;
  }

  get tiefkalt(): boolean {
    return this._tiefkalt;
  }

  set tiefkalt(value: boolean) {
    this._tiefkalt = value;
  }

  get vollbrand(): boolean {
    return this._vollbrand;
  }

  set vollbrand(value: boolean) {
    this._vollbrand = value;
  }
}
