import {Injectable} from '@angular/core';

@Injectable()
export class Feuerwehr {
  private _feuerwehrID: number;
  private _ort: string;
  private _feuerwehrStatusID: number;

  constructor(feuerwehrID: number, ort: string, feuerwehrStatusID: number) {
    this._feuerwehrID = feuerwehrID;
    this._ort = ort;
    this._feuerwehrStatusID = feuerwehrStatusID;
  }

  get feuerwehrID(): number {
    return this._feuerwehrID;
  }

  set feuerwehrID(value: number) {
    this._feuerwehrID = value;
  }

  get ort(): string {
    return this._ort;
  }

  set ort(value: string) {
    this._ort = value;
  }

  get feuerwehrStatusID(): number {
    return this._feuerwehrStatusID;
  }

  set feuerwehrStatusID(value: number) {
    this._feuerwehrStatusID = value;
  }
}
