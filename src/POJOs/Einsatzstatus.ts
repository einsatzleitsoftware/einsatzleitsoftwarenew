import {Injectable} from '@angular/core';

@Injectable()
export class Einsatzstatus {
  private _einsatzStatusID: number;
  private _status: string;

  get einsatzStatusID(): number {
    return this._einsatzStatusID;
  }

  set einsatzStatusID(value: number) {
    this._einsatzStatusID = value;
  }

  get status(): string {
    return this._status;
  }

  set status(value: string) {
    this._status = value;
  }
}
