exports.getZuteilungFromEinsatz = (con, einsatznummer, callback) => {
  const sql = `SELECT DISTINCT einsatzmittel.*, einsatzmittelzuteilung.zuteilungbis, einsatzmittelzuteilung.zuteilungvon, feuerwehr.ort FROM ((einsatz join einsatzmittelzuteilung on einsatz.einsatznummer = einsatzmittelzuteilung.einsatznummer) join einsatzmittel on einsatzmittelzuteilung.einsatzmittelID = einsatzmittel.einsatzmittelID) join feuerwehr on einsatzmittel.feuerwehrID = feuerwehr.feuerwehrID Where einsatz.einsatznummer = ${einsatznummer}`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: 'Cannot get any Einsatzmittelzuteilungen', found: false});
    }
  });
};
