exports.getEinsatzmittelzuteilung = (con, einsatznummer, callback) => {
  const sql = `SELECT einsatzmittel.*, einsatz.einsatznummer, feuerwehr.ort, einsatzmittelzuteilung.einsatzmittelzuteilungID FROM ((einsatzmittel join einsatzmittelzuteilung on einsatzmittel.einsatzmittelID = einsatzmittelzuteilung.einsatzmittelID) INNER JOIN feuerwehr ON einsatzmittel.feuerwehrID = feuerwehr.feuerwehrID) JOIN einsatz on einsatzmittelzuteilung.einsatznummer = einsatz.einsatznummer where einsatzmittelzuteilung.zuteilungbis is null AND einsatz.einsatznummer = ${einsatznummer}`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: null, found: true});
    }
  });
};
