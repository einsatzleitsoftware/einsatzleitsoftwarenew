exports.getFeuerwehren = (con, callback) => {
  const sql = `SELECT * from feuerwehr where feuerwehrStatusID = 1;`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: 'Cannot find Feuerwehr', found: false});
    }
  });
};
