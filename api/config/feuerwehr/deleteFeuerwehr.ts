exports.deleteFeuerwehr = (con, id, callback) => {
  const checkIfAllowed = `select count(*) as 'anz' from einsatzmittel where feuerwehrID=${id} and einsatzmittelStatusID != 4`;
  con.query(checkIfAllowed, (error, result) => {
    if (result[0].anz === 0) {
      const sql = `update feuerwehr set feuerwehrStatusID = 2 where feuerwehrID=${id}`;
      con.query(sql, (err, res) => {
        if (err) {
          throw err;
        }
        callback(true);
      });
    } else {
        callback(false);
    }
  });


};
