exports.postFeuerwehr = (con, ort, callback) => {
  const sql = `INSERT INTO feuerwehr (ort, feuerwehrStatusID) values('${ort}', 1)`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result) {
      callback({retVal: result.insertId, found: true});
    } else {
      callback({retVal: 'Cannot insert Feuerwehr', found: false});
    }
  });
};
