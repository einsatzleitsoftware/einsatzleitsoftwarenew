exports.getMF = (con, un, callback) => {
  let mf = -1;
  const sql = `SELECT mfactor FROM stoffe WHERE un='${un}';`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      mf = result[0].mfactor;
      callback({retVal: mf, found: true});
    } else {
      callback({retVal: 'Cannot find MassFactor to UN Number', found: false});
    }
  });
};
