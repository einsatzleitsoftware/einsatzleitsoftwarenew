exports.getPAC = (con, un, callback) => {
  let pac = -1;
  const sql = `SELECT pac from stoffe where un = '${un}';`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      pac = result[0].pac;
      callback({retVal: pac, found: true});
    } else {
      callback({retVal: 'Cannot find PAC to UN Number', found: false});
    }
  });
};
