exports.getWinkel = (con, kl, callback) => {
  let winkel = -1;
  const sql = `SELECT winkel FROM ausbreitungswinkel WHERE kl='${kl}';`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      winkel = result[0].winkel;
      callback({retVal: winkel, found: true});
    } else {
      callback({retVal: 'Cannot find Winkel to sent kl', found: false});
    }
  });
};
