exports.putAktuelleinsassenZugeteilt = (con, aIn, zug, emId, status, callback) => {
  const sql = `UPDATE einsatzmittel SET aktuelleInsassen=${aIn}, zugeteilt=${zug}, einsatzmittelStatusID=${status} WHERE einsatzmittelID = ${emId}`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    callback();
  });
};
