exports.getEinsatzmittel = (con, callback) => {
  const sql = `SELECT * from einsatzmittel where einsatzmittelStatusID != 4;`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: 'Cannot find Einsatzmittel', found: false});
    }
  });
};
