exports.postEinsatzmittel = (con, name, kbz, insassen, aktInsassen, status, feuerwehrID, notizen, callback) => {
  const sql = `INSERT INTO einsatzmittel (einsatzmittelName, kurzbezeichnung, insassen, aktuelleInsassen, einsatzmittelStatusID, feuerwehrID, zugeteilt, notizen) values('${name}', '${kbz}', ${insassen}, ${aktInsassen}, ${status}, ${feuerwehrID}, false, '${notizen}')`;

  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result) {
      callback({retVal: result.insertId, found: true});
    } else {
      callback({retVal: 'Cannot insert Einsatzmittel', found: false});
    }
  });
};
