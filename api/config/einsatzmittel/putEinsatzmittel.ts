exports.putEinsatzmittel = (con, id, name, kbz, insassen, aktInsassen, status, feuerwehrID, notizen, callback) => {
  let sql = `UPDATE einsatzmittel SET einsatzmittelName='${name}', kurzbezeichnung='${kbz}', insassen=${insassen},aktuelleInsassen=${aktInsassen}, einsatzmittelStatusID=${status}, feuerwehrID=${feuerwehrID}, zugeteilt= false, notizen='${notizen}' `;
  sql += ` WHERE einsatzmittelID=${id}`;
  con.query(sql, (err) => {
    if (err) {
      throw err;
    }
    callback();
  });
};
