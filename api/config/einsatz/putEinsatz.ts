exports.putEinsatz = (con, einsatzstichwort, notfallgeschehen, sachverhalt, alarmierendesEinsatzmittel, objekt, strasse, hausnummer, PLZ, ortstadt, einsatzStatusID, LSZAlarmierung, eingangszeit, startzeit, endzeit, einsatznummer, lat, lng, notizen, callback) => {

  let sql = `UPDATE einsatz SET einsatzstichwort='${einsatzstichwort}', notfallgeschehen='${notfallgeschehen}', sachverhalt='${sachverhalt}', alarmierendesEinsatzmittel='${alarmierendesEinsatzmittel}', objekt='${objekt}', strasse='${strasse}', hausnummer=${hausnummer}, PLZ=${PLZ}, ortstadt='${ortstadt}', einsatzStatusID=${einsatzStatusID}, LSZAlarmierung=${LSZAlarmierung}, eingangszeit='${eingangszeit}', lat=${lat}, lng=${lng}, notizen='${notizen}'`;
  startzeit != null ? sql += `, startzeit='${startzeit.slice(0, 19)}'` : sql += ``;
  endzeit != null ? sql += `, endzeit='${endzeit.slice(0, 19)}'` : sql += ``;
  sql += ` WHERE einsatznummer=${einsatznummer}`;

  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    callback();
  });
};
