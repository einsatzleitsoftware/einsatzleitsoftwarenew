exports.getEinsatzOpen = (con, callback) => {
  const sql = `SELECT * FROM einsatz WHERE einsatzStatusID = 1;`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: null, found: false});
    }
  });
};
