exports.putAdresseKoordinaten = (con, PLZ, strasse, hausnr, ortstadt, lat, lng, einsatznummer, callback) => {
  const sql = `UPDATE einsatz SET strasse = '${strasse}', hausnummer = ${hausnr}, ortstadt = '${ortstadt}', PLZ = ${PLZ}, lat = ${lat}, lng = ${lng} WHERE einsatznummer = ${einsatznummer}`;
  con.query(sql, (err) => {
    if (err) {
      throw err;
    }
    callback();
  });
};
