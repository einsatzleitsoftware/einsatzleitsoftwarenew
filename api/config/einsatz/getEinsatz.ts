exports.getEinsatz = (con, callback) => {
  const sql = `SELECT * FROM einsatz;`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: null, found: false});
    }
  });
};
