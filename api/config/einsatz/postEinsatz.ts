exports.postEinsatz = (con, einsatzstichwort, notfallgeschehen, sachverhalt, alarmierendesEinsatzmittel, objekt, strasse, hausnummer, PLZ, ortstadt, einsatzStatusID, LSZAlarmierung, eingangszeit, lat, lng, notizen, callback) => {
  const sql = `INSERT INTO einsatz (einsatzstichwort, notfallgeschehen, sachverhalt, alarmierendesEinsatzmittel, objekt, strasse, hausnummer, PLZ, ortstadt, einsatzStatusID, LSZAlarmierung, eingangszeit, lat, lng, notizen) values('${einsatzstichwort}', '${notfallgeschehen}', '${sachverhalt}', '${alarmierendesEinsatzmittel}', '${objekt}', '${strasse}', ${hausnummer}, ${PLZ}, '${ortstadt}', ${einsatzStatusID}, ${LSZAlarmierung}, '${eingangszeit}', ${lat}, ${lng}, '${notizen}')`;
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }

    if (result) {
      callback({einsatznummer: result.insertId, found: true});
    } else {
      callback({einsatznummer: -1, found: false});
    }
  });
};
