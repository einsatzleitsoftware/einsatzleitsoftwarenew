exports.getEinsatzmittelUndFeuerwehr = (con, callback) => {
  const sql = 'SELECT * from einsatzmittel INNER JOIN feuerwehr on einsatzmittel.feuerwehrID = feuerwehr.feuerwehrID';
  con.query(sql, (err, result) => {
    if (err) {
      throw err;
    }
    if (result[0]) {
      callback({retVal: result, found: true});
    } else {
      callback({retVal: 'Cannot join Tables Einsatzmittel and Feuerwehr', found: false});
    }
  });
};
