// NUESSLER
const getPAC = require('../config/nuessler/getPAC');
const getMF = require('../config/nuessler/getMF');
const getRadius = require('../config/nuessler/getRadius');
const getWinkel = require('../config/nuessler/getWinkel');
const checkUN = require('../config/nuessler/checkUN');
const postNuesselerErgebnis = require('../config/nuessler/postNuesslerErgebnis');
const getNuesslerErgebnisByEinsatzNr = require('../config/nuessler/getNuesslerErgebnisByEinsatzNr');

// EINSATZMITTEL UND FEUERWEHR
const getEinsatzmittelUndFeuerwehr = require('../config/einsatzmittelfeuerwehr/getEinsatzmittelUndFeuerwehr');

// EINSATZMITTELZUTEILUNG
const postEinsatzmittelzuteilung = require('../config/einsatzmittelzuteilung/postEinsatzmittelzuteilung');
const getEinsatzmittelzuteilung = require('../config/einsatzmittelzuteilung/getEinsatzmittelzuteilung');
const putEinsatzmittelzuteilungEndzeit = require('../config/einsatzmittelzuteilung/putEinsatzmittelzuteilungEndzeit');
const getZuteilungFromEinsatz = require('../config/einsatzmittelzuteilung/getZuteilungFromEinsatz');

const putAktuelleinsassenZugeteilt = require('../config/einsatzmittel/putAktuelleinsassenZugeteilt');

const getEinsatz = require('../config/einsatz/getEinsatz');
const postEinsatz = require('../config/einsatz/postEinsatz');
const putEinsatz = require('../config/einsatz/putEinsatz');
const deleteEinsatz = require('../config/einsatz/deleteEinsatz');
const putStatusStartZeit = require('../config/einsatz/putStatusStartZeit');
const getEinsatzByID = require('../config/einsatz/getEinsatzByID');

const getEinsatzmittel = require('../config/einsatzmittel/getEinsatzmittel');
const postEinsatzmittel = require('../config/einsatzmittel/postEinsatzmittel');
const putEinsatzmittel = require('../config/einsatzmittel/putEinsatzmittel');
const deleteEinsatzmittel = require('../config/einsatzmittel/deleteEinsatzmittel');
const getFeuerwehren = require('../config/feuerwehr/getFeuerwehren');
const deleteFeuerwehr = require('../config/feuerwehr/deleteFeuerwehr');
const postFeuerwehr = require('../config/feuerwehr/postFeuerwehr');
const putEinsatzmittelstatus = require('../config/einsatzmittel/putEinsatzmittelstatus');
const putFeuerwehr = require('../config/feuerwehr/putFeuerwehr');

const putAdresseKoordinaten = require('../config/einsatz/putAdresseKoordinaten');
const getEinsatzOpen = require('../config/einsatz/getEinsatzOpen');
const getEinsatzInProgress = require('../config/einsatz/getEinsatzInProgress');

module.exports = (app, con) => {

  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/dist/Einsatzleitsoftware/index.html');
  });


  // ------------------ NUESSLER ----------------------

  app.get('/getPAC', (req, res) => {
    res.type('json');
    const un = req.query.un;
    getPAC.getPAC(con, un, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/getMF', (req, res) => {
    res.type('json');
    const un = req.query.un;
    getMF.getMF(con, un, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/getRadius', (req, res) => {
    res.type('json');
    const kl = req.query.kl;
    const pac = req.query.pac;
    const menge = req.query.menge;
    getRadius.getRadius(con, kl, pac, menge, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/getWinkel', (req, res) => {
    res.type('json');
    const kl = req.query.kl;
    getWinkel.getWinkel(con, kl, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/checkUN', (req, res) => {
    res.type('json');
    const un = req.query.un;
    checkUN.checkUN(con, un, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.post('/postNuesslerErgebnis', (req, res) => {
    const ergAussen = req.body.ergAussen;
    const ergInnen = req.body.ergInnen;
    const berechnung = req.body.berechnung;
    const winkel = req.body.winkel;
    const klasse = req.body.klasse;
    const zeitstempel = req.body.zeitstempel.slice(0, 16);
    const einsatznummer = req.body.einsatznummer;
    const unNummer = req.body.unNummer;
    const menge = req.body.menge;
    const selection = req.body.selection;
    const vollbrand = req.body.vollbrand;
    const tiefkalt = req.body.tiefkalt;

    res.type('json');
    postNuesselerErgebnis.postNuesslerErgebnis(con, ergAussen, ergInnen, berechnung, winkel, klasse, zeitstempel, einsatznummer, unNummer, menge, selection, vollbrand, tiefkalt, (retVal) => {
      if (retVal.found){
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/getNuesslerErgebnisByEinsatzNr/:einsatznummer', (req, res) => {
    const einsatznummer = req.params.einsatznummer;

    res.type('json');
    getNuesslerErgebnisByEinsatzNr.getNuesslerErgebnisByEinsatzNr(con, einsatznummer, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(200).send(retVal);
      }
    });
  });

  // -------------------- NUESSLER ENDE ---------------

  // ------------------ EINSATZMITTEL ----------------------

  app.get('/getEinsatzmittel', (req, res) => {
    res.type('json');
    getEinsatzmittel.getEinsatzmittel(con, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.post('/postEinsatzmittel', (req, res) => {
    const name = req.body._einsatzmittelName;
    const kbz = req.body._kurzbezeichnung;
    const insassen = req.body._insassen;
    const aktInsassen = req.body._aktuelleInsassen;
    const status = req.body._einsatzmittelStatusID;
    const feuerwehrID = req.body._feuerwehrID;
    const notizen = req.body._notizen;

    res.type('json');
    postEinsatzmittel.postEinsatzmittel(con, name, kbz, insassen, aktInsassen, status, feuerwehrID, notizen, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.put('/putEinsatzmittel', (req, res) => {
    const id = req.body._einsatzmittelID;
    const name = req.body._einsatzmittelName;
    const kbz = req.body._kurzbezeichnung;
    const insassen = req.body._insassen;
    const aktInsassen = req.body._aktuelleInsassen;
    const status = req.body._einsatzmittelStatusID;
    const feuerwehrID = req.body._feuerwehrID;
    const notizen = req.body._notizen;

    putEinsatzmittel.putEinsatzmittel(con, id, name, kbz, insassen, aktInsassen, status, feuerwehrID, notizen, () => {
      res.end();
    });
  });

  app.put('/deleteEinsatzmittel', (req, res) => {
    const id = req.body.einsatzmittelID;
    deleteEinsatzmittel.deleteEinsatzmittel(con, id, () => {
      res.end();
    });
  });


  app.put('/putAktuelleinsassenZugeteilt', (req, res) => {
    const aIn = req.body.aktuelleInsassen;
    const zug = req.body.zugeteilt;
    const emId = req.body.einsatzmittelID;
    const status = req.body.einsatzStatusID;

    putAktuelleinsassenZugeteilt.putAktuelleinsassenZugeteilt(con, aIn, zug, emId, status, () => {
      res.end();
    });
  });

  app.put('/putEinsatzmittelstatus', (req, res) => {
    const emId = req.body.emId;

    putEinsatzmittelstatus.putEinsatzmittelstatus(con, emId, () => {
      res.end();
    });
  });


  // -------------------- EINSATZMITTEL ENDE ---------------

  // -------------------- FEUERWEHR ---------------
  app.get('/getFeuerwehren', (req, res) => {
    res.type('json');
    getFeuerwehren.getFeuerwehren(con, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.put('/deleteFeuerwehr', (req, res) => {
    const id = req.body.feuerwehrID;
    deleteFeuerwehr.deleteFeuerwehr(con, id, (retVal) => {
       res.send(retVal);
    });
  });

  app.post('/postFeuerwehr', (req, res) => {
    const ort = req.body._ort;
    res.type('json');
    postFeuerwehr.postFeuerwehr(con, ort, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.put('/putFeuerwehr', (req, res) => {
    const id = req.body._feuerwehrID;
    const name = req.body._ort;

    putFeuerwehr.putFeuerwehr(con, id, name, () => {
      res.end();
    });
  });
  // -------------------- FEUERWEHR ENDE ---------------


// ----------- EINSATZMITTEL UND FEUERWEHR ----------

  app.get('/getEinsatzmittelUndFeuerwehr', (req, res) => {
    res.type('json');
    getEinsatzmittelUndFeuerwehr.getEinsatzmittelUndFeuerwehr(con, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

// -------- EINSATZMITTEL UND FEUERWEHR ENDE --------

// ---------- EINSATZMITTELZUTEILUNG ----------------

  app.post('/postEinsatzmittelzuteilung', (req, res) => {
    const emID = req.body.einsatzmittelID;
    const einnum = req.body.einsatznummer;
    const zvon = req.body.zuteilungvon;

    postEinsatzmittelzuteilung.postEinsatzmittelzuteilung(con, emID, einnum, zvon, () => {
      res.end();
    });
  });

  app.get('/getEinsatzmittelzuteilung/:einsatznummer', (req, res) => {
    const einsatznummer = req.params.einsatznummer;

    res.type('json');
    getEinsatzmittelzuteilung.getEinsatzmittelzuteilung(con, einsatznummer, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.put('/putEinsatzmittelzuteilungEndzeit', (req, res) => {
    const einzuID = req.body.einzuID;
    const endZeit = req.body.endZeit.slice(0, 19);
    putEinsatzmittelzuteilungEndzeit.putEinsatzmittelzuteilungEndzeit(con, einzuID, endZeit, () => {
      res.end();
    });
  });

  app.get('/getZuteilungFromEinsatz/:einsatznummer', (req, res) => {
    res.type('json');
    getZuteilungFromEinsatz.getZuteilungFromEinsatz(con, req.params.einsatznummer, (retVal) => {
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

// ---------- EINSATZMITTELZUTEILUNG ENDE -----------


// ------------------ EINSATZ ----------------------

  app.get('/getEinsatz', (req, res) => {
    getEinsatz.getEinsatz(con, (retVal) => {
      res.type('json');
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(200).send(retVal);
      }
    });
  });

  app.get('/getEinsatzOpen', (req, res) => {
    getEinsatzOpen.getEinsatzOpen(con, (retVal) => {
      res.type('json');
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.get('/getEinsatzInProgress', (req, res) => {
    getEinsatzInProgress.getEinsatzInProgress(con, (retVal) => {
      res.type('json');
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.post('/postEinsatz', (req, res) => {
    const einsatzstichwort = req.body._einsatzstichwort;
    const notfallgeschehen = req.body._notfallgeschehen;
    const sachverhalt = req.body._sachverhalt;
    const alarmierendesEinsatzmittel = req.body._alarmierendesEinsatzmittel;
    const objekt = req.body._objekt;
    const strasse = req.body._strasse;
    const hausnummer = req.body._hausnummer;
    const PLZ = req.body._PLZ;
    const ortstadt = req.body._ortstadt;
    const einsatzStatusID = req.body._einsatzStatusID;
    const LSZAlarmierung = req.body._LSZAlarmierung;
    const eingangszeit = req.body._eingangszeit.slice(0, 19);
    const lat = req.body._lat;
    const lng = req.body._lng;
    const notizen = req.body._notizen;

    postEinsatz.postEinsatz(con, einsatzstichwort, notfallgeschehen, sachverhalt, alarmierendesEinsatzmittel, objekt, strasse, hausnummer, PLZ, ortstadt, einsatzStatusID, LSZAlarmierung, eingangszeit, lat, lng, notizen, (retVal) => {
      res.type('json');
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });

  app.put('/putEinsatz', (req, res) => {
    const einsatzstichwort = req.body._einsatzstichwort;
    const notfallgeschehen = req.body._notfallgeschehen;
    const sachverhalt = req.body._sachverhalt;
    const alarmierendesEinsatzmittel = req.body._alarmierendesEinsatzmittel;
    const objekt = req.body._objekt;
    const strasse = req.body._strasse;
    const hausnummer = req.body._hausnummer;
    const PLZ = req.body._PLZ;
    const ortstadt = req.body._ortstadt;
    const einsatzStatusID = req.body._einsatzStatusID;
    const LSZAlarmierung = req.body._LSZAlarmierung;
    const eingangszeit = req.body._eingangszeit.slice(0, 19);
    const startzeit = req.body._startzeit;
    const endzeit = req.body._endzeit;
    const einsatznummer = req.body._einsatznummer;
    const lat = req.body._lat;
    const lng = req.body._lng;
    const notizen = req.body._notizen;


    putEinsatz.putEinsatz(con, einsatzstichwort, notfallgeschehen, sachverhalt, alarmierendesEinsatzmittel, objekt, strasse, hausnummer, PLZ, ortstadt, einsatzStatusID, LSZAlarmierung, eingangszeit, startzeit, endzeit, einsatznummer, lat, lng, notizen, () => {
      res.end();
    });
  });

  app.delete('/deleteEinsatz/:delID', (req, res) => {
    const delID = req.params.delID;

    getZuteilungFromEinsatz.getZuteilungFromEinsatz(con, delID, (retVal) => {
      if (retVal.found) {
        for (const data of retVal.retVal) {
          putAktuelleinsassenZugeteilt.putAktuelleinsassenZugeteilt(con, 0, 0, data.einsatzmittelID, 2, () => {
          });
        }
      }
    });
    deleteEinsatz.deleteEinsatz(con, delID, () => {
      res.end();
    });
  });

  app.put('/putStatusStartZeit', (req, res) => {
    const einsatzStatusID = req.body.einsatzStatusID;
    const startzeit = req.body.startZeit.slice(0, 16);
    const einsatznummer = req.body.einsatznummer;

    putStatusStartZeit.putStatusStartZeit(con, einsatzStatusID, startzeit, einsatznummer, () => {
      res.end();
    });
  });

  app.put('/putAdresseKoordinaten', (req, res) => {
      const PLZ = req.body.PLZ;
      const strasse = req.body.strasse;
      const hausnr = req.body.hausnr;
      const ortstadt = req.body.ortstadt;
      const lat = req.body.lat;
      const lng = req.body.lng;
      const einsatznummer = req.body.einsatznummer;

      putAdresseKoordinaten.putAdresseKoordinaten(con, PLZ, strasse, hausnr, ortstadt, lat, lng, einsatznummer, () => {
        res.end();
      });
  });


  app.get('/getEinsatzByID/:einsatznummer', (req, res) => {
    const einsatznummer = req.params.einsatznummer;

    getEinsatzByID.getEinsatzByID(con, einsatznummer, (retVal) => {
      res.type('json');
      if (retVal.found) {
        res.status(200).send(retVal);
      } else {
        res.status(400).send(retVal);
      }
    });
  });
// -------------------- Einsatz ENDE ---------------


  app.get('/*', (req, res) => {
    res.sendFile(__dirname + '/dist/Einsatzleitsoftware/index.html', (err) => {
      if (err) {
        res.status(500).send(err);
      }
    });
  });
};

