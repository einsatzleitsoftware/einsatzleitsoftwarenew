const http = require('http');
const mysql = require('mysql');
const express = require('express');
const cors = require('cors');
const port = 5000;
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
const poolCluster = mysql.createPoolCluster();
let con = null;

poolCluster.add('schule', {
  host: '10.2.1.16',
  user: 'ffein',
  password: 'htlpinkafeld',
  database: 'ffein'
});

poolCluster.add('lokal', {
  host: 'localhost',
  user: 'ffein',
  password: 'htlpinkafeld',
  database: 'ffein'
});


poolCluster.getConnection((err, connection) => {
  if (err) {
    console.log(err);
  } else {
    con = connection;
    console.log('Connected!');
    require('./api/routes/routes')(app, con);
  }
});


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist/Einsatzleitsoftware'));



app.listen(port, () => {
  console.log(`Einsatzleitsoftware app listening at http://localhost:${port}`);
});
