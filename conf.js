exports.config = {
  framework: 'jasmine',
  baseUrl: 'http://localhost:4200',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['src/app/einsatzverwaltungModul/components/einsatzverwaltung/einsatzverwaltung.component.spec.ts']
}
